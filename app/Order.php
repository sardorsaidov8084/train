<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\OrderStatus;

class Order extends Model
{
    protected $fillable =[
        'user_id',
        'company_id',
        'address',
        'phone',
        'amount',
        'created_by'
    ];
    const STATUES = [
        'pokupatel'  => 'Покупатель',
        'my_orders'   => 'Мои заказы',
        'agent' => 'Посредник',
        'create_orders' => 'Мои заказы',
        
    ];
   
    /**
     *
     */
    const MY_ORDERS =  'my_orders';
    /**
     *
     */
    const AGENT = 'agent';
    const POKUPATEL ='pokupatel';
    const CONSUMER = 'consumer';
    // const STATUS = 1;
    // const NO_STATUS = 0;
    public function status(){
        return $this->belongsTo(OrderStatus::class,'id','order_id')->withoutGlobalScope('state');
    }
    public function user(){

        return $this->belongsTo('App\User','created_by','id');
    }
    public function createdUser()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
    public function products(){

        return $this->belongsToMany('App\Product','order_products');
    }
    public function orderProducts()
    {
        return $this->hasMany(OrderProduct::class);
    }
    public function companies(){

        return $this->belongsTo('App\Company','company_id');
    }

    public function orderTransactions()
    {
        return $this->hasMany('App\OrderTransaction');
        
    }

    public function transactionAmount($id)
    {
        $order = Order::find($id);
        $amount=0;
          foreach ($order->orderTransactions as $orderTransaction) {
                $amount += $orderTransaction->transaction->amount;
            } 
        return $amount;
    }
}
