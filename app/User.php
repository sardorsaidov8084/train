<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
   
    use Notifiable;
    use \App\Http\Controllers\Traits\UserTraits;
   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    const NO_VERIFIED = 0;
    const VERIFIED = 1;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'email', 'password','phone','company_id', 'verified','cash',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function company()
    {
        return $this->belongsTo('App\Company','company_id','id');
    }

    public function createdTransactions()
    {
        return $this->hasMany('App\UserTransaction','created_by');
    }

    public function userTransactions()
    {
        return $this->hasMany('App\UserTransaction','user_id');
    }

    public function createdAll()
    {
        $i=0;
        foreach (auth()->user()->createdTransactions as $createdTransaction) {
            
            $transaction[$i] = $createdTransaction->transaction;
            $i++;
        }
        // $order = Order::find($id);
        // $amount=0;
        //   foreach ($order->orderTransactions as $orderTransaction) {
        //         $amount += $orderTransaction->transaction->amount;
        //     } 
        return $transaction;
    }

    public function userAll()
    {

    }

    public function income($qiymat)
    {
        $amount=0;
        if($qiymat){
            $userTransactions = auth()->user()->userTransactions;
            if(empty($userTransactions))
            {
                return 0;
            }
            foreach ($userTransactions as $userTransaction) {
                $amount+=$userTransaction->transaction->amount;
            }
            return $amount;
        }else
        {
            $createdTransactions = auth()->user()->createdTransactions;
            if(empty($createdTransactions))
            {
                return 0;
            }
            foreach ($createdTransactions as $createdTransaction) {
                $amount += $createdTransaction->transaction->amount;
            }
            return $amount;
        }
    }

    public function debtor()
    {
        $amount=0;
        $banks = Bank::where('created_by',auth()->id())->get();
        foreach ($banks as $bank) {
            $amount +=$bank->amount;
        }

        return $amount;
    }

    public function debt()
    {
        $amount=0;
        $banks = Bank::where('user_id',auth()->id())->get();
        foreach ($banks as $bank) {
            $amount +=$bank->amount;
        }

        return $amount;
    }
   
}
