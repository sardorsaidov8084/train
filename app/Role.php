<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{    
    protected $fillable = ['name','slug', 'description','level'];

    const ADMIN = 'administrator';
    const USER =    'user';
    const POKUPATEL = 'pokupatel';
    const DOSTAVKA = 'dostavka';
    const AGENT =  'agent';

    
    const DEFAULT_LEVEL = 1;
   

   
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions(){
        return $this->belongsToMany('App\Permission','permission_role');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User','role_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|mixed
     */
    public function getPermissions()
    {
        return (!$this->permissions) ? $this->permissions()->get() : $this->permissions;
    }

    /**
     * @param null $permissionName
     * @return mixed
     */
    public function getPermission($permissionName = null)
    {
        return \App\Permission::where('slug',$permissionName)->first();
    }

    /**
     * @param $permission
     * @return bool
     */
    public function hasPermission($permission)
    {
        return $this->getPermissions()->contains($this->getPermission($permission));
    }

    /**
     * @param $permission
     * @return bool|void
     */
    public function attachPermission($permission)
    {
        return (!$this->getPermissions()->contains($permission)) ? $this->permissions()->attach($permission) : true;
    }

    /**
     * @param $permission
     */
    public function detachPermission($permission)
    {
        $this->permissions = null;
        $this->permissions()->detach($permission);
    }

    /**
     *
     */
    public function detachAllPermissions()
    {
        $this->permissions = null;
        $this->permissions()->detach();
    }

    public function scopeRole($query,$role)
    {
        return $query->where('slug',$role);
    }
}
