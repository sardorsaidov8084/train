<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class Image extends Model
{
     static public function store($file = false, $image_folder = null, $id){
        /** @var TYPE_NAME $file */
       
        if (strpos($file->getMimeType(), 'image/') === false)
            return null;

        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
        $name = $timestamp . '-' . rand(0, 100) . '.' . $file->extension();
        $folder = 'admin/images/'.$image_folder.'/';
        $path = public_path($folder);
        $image = Image::find($id);
        if($image){
            $filename = public_path($image['path']);
            File::delete($filename);
            
        }
        if(is_null($image)){
            $image = new Image();
        }
        $file->move($path, $name);
        $image->path = $folder . $name;
        $image->type = $image_folder;
       
        if ($image->save())
            return $image;
        else
            return null;
        
        }
        public function delete_file($path=null){
            $folder = $path;
            $filename = public_path($folder);
            File::delete($filename);
        }
}
