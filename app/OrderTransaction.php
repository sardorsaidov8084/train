<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderTransaction extends Model
{
    protected $fillable = [
    	'order_id',
    	'transaction_id',
    	'created_by',
    ];

    public function transaction()
    {

    	return $this->belongsTo('App\Transaction','transaction_id','id');
    }
}
