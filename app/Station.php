<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Station extends Model
{
    
    protected $fillable = [
        'name',
        'country_id'
    ];
    public function country(){

        return $this->belongsTo('App\Country');
    }
    public function directions(){

        return $this->belongsToMany('App\Direction');
    }
}
