<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    
    protected $fillable = [
    	'name',
    	'description',
    	'address',
    	'phone',
    	'image_id'
    ];

    public function image(){

        return $this->belongsTo('App\Image');

    }

}
