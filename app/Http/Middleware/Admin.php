<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Role;
class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::check() ) {
            if(Auth::user()->hasRole(Role::AGENT)|| Auth::user()->hasRole(Role::DOSTAVKA)||Auth::user()->hasRole(Role::POKUPATEL))
            return $next($request);
        }
        return redirect()->route('admin.login');
    }
}