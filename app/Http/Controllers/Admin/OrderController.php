<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Brian2694\Toastr\Facades\Toastr;
use App\Order;
use App\Product;
use Response;
use App\OrderProduct;
use App\OrderStatus;
use App\Role;
use DB;
use App\Station;
use App\Train;
use App\Wagon;
use App\Container;
use App\Status;
use App\Shipping;
use App\User;
use App\Company;
use App\Transaction;
use App\UserTransaction;
use App\OrderTransaction;

use Illuminate\Database\Eloquent\Builder;
class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
       
        if(auth()->user()->hasRole(Role::DOSTAVKA)){
            $orders = Order::whereIn('id',OrderStatus::status(OrderStatus::AGENT)
                ->pluck('order_id')->toArray())->where('created_by',auth()->id())->latest('created_at')->get();   
               
        }
      
        if(auth()->user()->hasRole(Role::AGENT)){
            
            $orders = Order::where('user_id', '!=', auth()->id())->latest()->get();
            
        }
       
        return view('admin.orders.index', compact('orders'));
    }


    public function myOrdersIndex(){
        if(auth()->user()->hasRole(Role::POKUPATEL)||auth()->user()->hasRole(Role::AGENT)){
          $orders = Order::where(['user_id' => auth()->id()])->latest()->get();    
        }
        
        return view('admin.orders.my_orders.index',compact('orders'));
    }

    
   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $companies = Company::latest()->get();
        return view('admin.orders.create',compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
        
        if (!auth()->check()||(!$request['product'])){
            Toastr::error('Ma`lumotlar to`liq kiritilmadi');
            return back();
        }
       $order=Order::create([
            'user_id' => auth()->id(),
            'address' => $request['address'],
            'amount' =>  0,
            'created_by' => 0,
        ]);
        if(auth()->user()->hasRole(Role::AGENT)){
            $order->status()->create(['order_id' => $order->id,'body' => Status::find(1)->name,'status' =>Role::AGENT,'agent' =>'created','state' =>'created']);
        }
        else{
            $order->status()->create(['order_id' => $order->id,'body' => Status::find(1)->name,'status' =>Role::POKUPATEL,'state' =>'created']);
        }

        $amount = $this->orderProduct($request, $order);
        if($amount==0){
            Toastr::error('Ma`lumotlar to`liq kiritilmadi');
            return redirect()->back();
        }

        if(auth()->user()->hasRole(Role::POKUPATEL))
        {
            $role = Role::where(['slug'=>'agent'])->first();
            $user_id = DB::table('role_user')->where('role_id',$role->id)->value('user_id');
        }else{
            $user_id = $this->getCompanyId($request);
        }

        $order->amount = $amount;
        $order->created_by = $user_id;
        $order->update();
        if(auth()->user()->hasRole(Role::POKUPATEL)||auth()->user()->hasRole(Role::AGENT)){
            $orders = Order::where(['user_id' => auth()->id()])->latest()->get();    
          }
        
        return redirect()->route('admin.orders.my_orders.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($order)
    {   
        $orderProducts = OrderProduct::where(['order_id' =>$order->id ])->get();
    
        return view('admin.orders.show', compact('order','orderProducts'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($order)
    {
        
        $products = Product::latest()->get();
        $orderProducts = OrderProduct::where(['order_id' =>$order->id ])->get();
        return view('admin.orders.edit', compact('order','orderProducts','products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $order)
    {
       
        $orderProducts = OrderProduct::where(['order_id'=>$order->id])->get();

        foreach ($orderProducts as $orderProduct) {
            $orderProduct->delete();
        }
        $amount = $this->orderProduct($request, $order);
        if($amount==0){
            return redirect()->back();
        }
        $order->amount = $amount;
        $order->update();
        return redirect()->route('admin.orders.my_orders.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($order)
    {
     
        return redirect()->route('admin.orders.my_orders.index');
    }
    public function orderPrice($id = null){
        if($id == "")
        return Response::json(false);
        else{
            $order = Product::find($id);
            
            return Response::json($order);
        }
    }
    public function orderProduct($request, $order){
        $amount=0;
        if($request['product']){
            foreach ($request['product'] as $value) {
                $orderProduct = $order ->OrderProducts()->create([
                    'order_id' => $order->id,
                    'product_id' => $value['id'],
                    'company_id' => $request['company_id'],
                    'quantity'  => $value['quantity'],
                    'product_price' => $value['price'],
                    'amount' =>  $value['quantity']*$value['price'],
                     'weight' => $value['weight'],
                
                ]);
                $company = Company::find($request['company_id']);
                $amount +=  $orderProduct->amount;
            }
            return $amount;
        }
       
    }

    public function getCompanyId($request){
        if($request['company_id']){

            $user_id = User::where(['company_id'=>$request['company_id']])->first();
            
            return $user_id->id;
        }
       
    }

    public function orderFromStation($id = null){
        
        if($id != null){
            $stations= Station::where(['country_id' =>$id])->get();
            
                return Response::json($stations);
          
           
        }
        else{
            return Response::json(false);
        }
    }
    
    public function orderTrain($id = null){
      
       
        if($id != null){
            $trains= Train::latest()->get();
              
                return Response::json($trains);
          
        }
        else{
            return Response::json(false);
        }
       
    }

    public function orderWagon($id = null){
      
       
        if($id != null){
            $wagons= Wagon::where(['train_id'=>$id])->get();
              
                return Response::json($wagons);
          
        }
        else{
            return Response::json(false);
        }
       
    }
    public function orderContainer($id = null){
      
       
        if($id != null){
            $containers= Container::where(['wagon_id'=>$id])->get();
              
                return Response::json($containers);
          
        }
        else{
            return Response::json(false);
        }
       
    }

    // orderStatus lar bilan ishlash

    public function myOrdersConfirm(Request $request){
     
       
        $order = Order::find($request['order_id']);

        if (is_null($order))
        {
            return redirect()->back();
        }
 
        $order->status->update([
            'body'      => Status::find(3)->name,
           
        ]);       
        $order->update(); 

        return redirect()->route('admin.orders.show',$order->id);
    }

    public function myOrdersDelete(Request $request){
       
        
        $order = Order::find($request['order_id']);

        if (is_null($order))
        {
            return redirect()->back();
        }
        
            $order->status->update([
                'body'      => Status::find(2)->name,
                'delivery' => $request['description'],
                'state' => 'no_created',
                'agent' =>'no_created',
            ]);    
        
          
        $order->update(); 
        if(auth()->user()->hasRole(Role::POKUPATEL)||auth()->user()->hasRole(Role::AGENT)){
          
            $orders = Order::where(['user_id' => auth()->id()])->latest()->get();
            
            return view('admin.orders.my_orders.index',compact('orders'));
        }
        $orders = Order::whereIn('id',OrderStatus::status(OrderStatus::AGENT)
                ->pluck('order_id')->toArray())->latest('created_at')->get();   
               
         return redirect()->route('admin.orders.index',compact('orders'));
    }
    public function myOrdersWait(Request $request){

        $order = Order::find($request['order_id']);

        if (is_null($order))
        {
            return redirect()->back();
        }
 
        $order->status->update([
            'body'      => Status::find(4)->name,
           
        ]);       
        $order->update(); 

        return redirect()->route('admin.orders.show',$order->id);
    }
    public function myOrdersReady(Request $request){

        $order = Order::find($request['order_id']);

        if (is_null($order))
        {
            return redirect()->back();
        }
 
        $order->status->update([
            'body'      => Status::find(5)->name,
           
        ]);       
        $order->update(); 
        if(auth()->user()->hasRole(Role::AGENT)){
            
            $orders = Order::where('user_id', '!=', auth()->id())->latest()->get();
           //return redirect()->route('admin.orders.my_orders.index',compact('orders'));
        }
        
        return redirect()->route('admin.orders.index', compact('orders'));
    }
    public function orderComplete(Request $request){
       
       
        $order = Order::find($request['order_id']);

        if (is_null($order))
        {         
            return redirect()->back();
        }
        $status=Status::find(6);

        $order->status->update([
            'body'      => $status->name,
            'paid' => 'completed',
            'state' => 'no_created',
            'agent' =>'no_created',
        ]);    
        
        $order->update();


       

        if(auth()->user()->hasRole(Role::POKUPATEL)||auth()->user()->hasRole(Role::AGENT)){
            $orders = Order::where(['user_id' => auth()->id()])->latest()->get();
            Toastr::success('Был принят');
            return redirect()->route('admin.orders.my_orders.index',compact('orders'));
   
          }
          return redirect()->back();

    }
    public function canceleds(){
        
        $orders = Order::whereIn('id',OrderStatus::where(['body' => 'Заказ Отменен!'])
                ->pluck('order_id')->toArray())->latest('created_at')->get();  
               
        return view('admin.orders.canceleds.index',compact('orders'));
    }
    public function closed(){
        $orders = Order::whereIn('id',OrderStatus::where(['body' => 'Закрыто'])
        ->pluck('order_id')->toArray())->latest('created_at')->get();  
        return view('admin.orders.closed.index',compact('orders'));
    }
}
