<?php

namespace App\Http\Controllers\Admin;

use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Role;
use App\Order;
use App\OrderStatus;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request){
      
        if(Auth::attempt(['email' => $request['email'],'password' => $request['password']])){
            Toastr::success('Успешно!');
            return redirect()->route('admin.admin.index');
        }
        Toastr::error('Ошибка пароля или телефона!');
        return redirect()->back();
    }
    public function logout()
    {
        if(Auth::check())
            Auth::logout();

        return redirect()->route('admin.login');
    }
    public function index()
    {
        if(auth()->user()->hasRole(Role::DOSTAVKA)){
            $orders = Order::whereIn('id',OrderStatus::status(OrderStatus::AGENT)
                ->pluck('order_id')->toArray())->latest('created_at')->get();
        }

        if(auth()->user()->hasRole(Role::AGENT)){
            
            $orders = Order::where('user_id', '!=', auth()->id())->latest()->get();
        }

        if(auth()->user()->hasRole(Role::POKUPATEL)){
            $orders = Order::where(['user_id' => auth()->id()])->latest()->get();

            return view('admin.orders.my_orders.index',compact('orders'));    
        }

       return view('admin.orders.index',compact('orders')); 

    }
    public function createUser($request = null)
    {   
      
        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'verified' => User::NO_VERIFIED,
            'password' => bcrypt($request['password'])
        ]);
         auth()->login($user);
         return $user;
    }
   public function register(Request $request){
    $this->validate($request,[
        'name' => 'required|min:3|max:191',
        'email'=> 'required|unique:users',
        'phone' => 'required|unique:users|min:12|max:12',
        'password' => 'required|min:6|max:20',
        
    ]);
    
    $user= $this->createUser($request);

    return view('admin.login'); 
   }
}