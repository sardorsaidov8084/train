<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use Auth;
use App\Company;
use App\Transaction;
use App\OrderTransaction;
use App\UserTransaction;
use App\Order;
use App\Bank;
use App\BankUser;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createUser($request = null)
    {   
      
        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'verified' => User::NO_VERIFIED,
            'password' => bcrypt($request['password'])
        ]);
         auth()->login($user);
         return $user;
    }
    public function index()
    {
        $users = User::latest()->get();
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createRole($id)
    {

        $user  = User::find($id);
        $roles = Role::all();
        $user_roles = $user->roles;
      return view('admin.users.role',['user'=>$user,'roles'=>$roles,'user_roles'=>$user_roles]);
    }


    public function storeRole($id,$role){
           
          $user = User::find($id);
          $user->roles()->syncWithoutDetaching($role);
          return redirect()->route('admin.users.index');
    }

    public function detachRole($id,$role){
        $user = User::find($id);
        $user->detachRole($role);
        return redirect()->route('admin.users.index');
    }

    public function store(Request $request)
    {  
        $this->validate($request,[
            'name' => 'required|min:3|max:191',
            'email'=> 'required|unique:users',
            'phone' => 'required|unique:users|min:12|max:12',
            'password' => 'required|min:6|max:20',
            
        ]);
        
        $user= $this->createUser($request);

        return redirect()->route('admin.users.index');
    }
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $companies = Company::latest()->get();
        return view('admin.users.edit', compact('user','companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|min:3|max:191',
            'phone' => 'required|min:12|max:12',
            'company_id' => 'required',
        ]);
        
        User::where(['id' =>$id])->update(['name' =>$request['name'], 'phone' =>$request['phone'], 'company_id'=>$request['company_id']]);
        
        return redirect()->route('admin.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();

        return redirect()->route('admin.users.index');
    }
    public function list($slug){
        $role = Role::where('slug',$slug)->first();
        $users = $role->users;
        return view('admin.users.index',compact('users'));
    }

    public function account()
    {
        $user_transactions = UserTransaction::where(['user_id'=>auth()->id()])->latest()->get();
        $created_transactions = UserTransaction::where(['created_by'=>auth()->id()])->latest()->get();
        $orders = Order::latest()->get();
        $banks = Bank::latest()->get();
        $users = User::latest()->get();
        $bankUsers = BankUser::latest()->get();
        $transactions = Transaction::latest()->get();

        return view('admin.profil.account',compact('transactions','user_transactions','orders','created_transactions','banks','users','bankUsers'));
    }

}
