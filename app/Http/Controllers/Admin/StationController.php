<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Station;
use Response;

class StationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stations = Station::latest()->get();
        return view('admin.stations.index', compact('stations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('admin.stations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'country_id' => 'required',
        ]);
        Station::create([
            'name' => $request['name'],
            'country_id' => $request['country_id'],
        ]);
        return redirect()->route('admin.stations.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($station)
    {
      return view('admin.stations.edit', compact('station'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $station)
    {
        $this->validate($request,[
            'name' => 'required',
            'country_id' => 'required',
        ]);
        Station::where(['id' =>$station->id])->update([
            'name' => $request['name'],
            'country_id' => $request['country_id'],
        ]);
        return redirect()->route('admin.stations.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($station)
    {
        $station->delete();
        return redirect()->route('admin.stations.index');
    }
    public function select(){
       
       $stations = Station::get();
        return Response::json($stations);
    }
}
