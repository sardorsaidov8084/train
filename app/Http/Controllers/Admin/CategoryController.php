<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Image;
use App\Role;
use App\Product;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $categories = Category::latest()->get();
        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!auth()->user()->hasRole(Role::AGENT))
        {
            return redirect()->route('admin.categories.index');
        }
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $this->validate($request,[
            'name'        => 'required',
            'image'      => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
          ]);
 
         $image_id = null;
         if ($request->hasFile('image'))
         {
             $file = $request->image;
             $image = Image::store($file,'category',null,null);
             if (!is_null($image))
             {
                 $image_id = $image['id'];
             }
         }
 
         //$parent = Category::find($request['parent_id']);
 
         $category = Category::create([
             'name'          => $request['name'],
             'description'   => $request['description'],
             'image_id'      => $image_id,
             'status'        => (isset($request['status']))?Category::STATUS:Category::NO_STATUS,
             'parent_id'     => $request['parent_id'],
         ]);
        //  if(!is_null($parent))
        //   {   
        //      $parent->appendNode($category);
        //   }
        //  $this->storeAttributes($category,$request);
         return redirect()->route('admin.categories.index');
     }
 

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($category)
    {
        $category = Category::find($category->id);
        $products = Product::where(['category_id'=>$category->id])->get();

        return view('admin.categories.show',compact('category','products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($category)
    {
        if(!auth()->user()->hasRole(Role::AGENT))
        {
            return redirect()->route('admin.categories.index');
        }
        return view('admin.categories.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $category)
    {
     
       
       $this->validate($request,[
        'name'        => 'required',
        'image'      => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
       ]);
      
       $image_id = null;
       if ($request->hasFile('image'))
       {
           $file = $request->image;
           $image = Image::store($file,'category',$category->image_id);
           if (!is_null($image))
           {
               $image_id = $image['id'];
           }
       }
       if($image_id){
            $category->image_id = $image_id;
        }
        else{
            $category->image_id = $category->image_id;
        }

        $category->name =  $request['name'];
        $category->description =  $request['description'];
       
        $category->status = (isset($request['status']))?Category::STATUS:Category::NO_STATUS;
        $category->parent_id = $request['parent_id'];
        $category->update();
        return redirect()->route('admin.categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($category)
    {   $image = Image::find($category->image_id);
        if($image){
            $image->delete_file($image->path);
            $image->delete();
        }
        $category->delete();
        
        return redirect()->route('admin.categories.index');
    }
}
