<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Container;
use App\Image;
class ContainerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $containers = Container::latest()->get();
        
        return view('admin.containers.index', compact('containers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.containers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'order_id' => 'required',
            'wagon_id' => 'required',
            'image'      => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $image_id = null;
       if ($request->hasFile('image'))
       {
           $file = $request->image;
           $image = Image::store($file,'container',null,null);
           if (!is_null($image))
           {
               $image_id = $image['id'];
           }
       }
       Container::create([
           'name' => $request['name'],
           'order_id' => $request['order_id'],
           'wagon_id' => $request['wagon_id'],
           'image_id' => $image_id,
       ]);
       return redirect()->route('admin.containers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($container)
    {
        return view('admin.containers.edit', compact('container'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $container)
    {
        $this->validate($request,[
            'order_id' => 'required',
            'wagon_id' => 'required',
            'image'     => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $image_id = null;
        Container::where(['id' =>$container->id])->update([
           'name' => $request['name'],
           'order_id' => $request['order_id'],
           'wagon_id' => $request['wagon_id'],
           'image_id' => $image_id,
        ]);
        return redirect()->route('admin.containers.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($container)
    {
        $container->delete();
        return redirect()->route('admin.containers.index');
    }
}
