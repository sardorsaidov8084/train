<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transaction;
use App\User;
use App\UserTransaction;
use App\OrderTransaction;
use App\Order;
use App\Role;
use DB;
use Session;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $order = Order::find($request['order_id']);
        $created_user = User::find(auth()->id());
        $user = User::find($order->created_by);
        if($order->amount>=$request['payment'] && $created_user->cash>=$request['payment'])
        {
            $order_transaction_new = OrderTransaction::where(['order_id'=>$order->id])->get();
            if($order_transaction_new)
            {

                if($order->status->paid!="payment_complated")
                {

                    $amount = $order->amount-$order->transactionAmount($order->id);
                
                    if($amount>=$request['payment'])
                    {
                        // $transaction =  auth()->user()->createdAll();
                        // return count($transaction);

                    $transaction1 = Transaction::create([
                        'amount' => $request['payment'],
                        'income' => false,
                        'currency' => "UZB",
                        'accepted' => true,
                    ]);

                    $transaction2 = Transaction::create([
                        'amount' => $request['payment'],
                        'income' => true,
                        'currency' => "UZB",
                        'accepted' => false,
                    ]);


                    $created_user->cash = $created_user->cash-$transaction1->amount;
                    $created_user->update();

                    $user->cash = $user->cash+$transaction2->amount;
                    $user->update();

                    $user_transaction1 = UserTransaction::create([
                        'user_id' => $user->id,
                        'transaction_id' => $transaction1->id,
                        'created_by' => $created_user->id,
                    ]);

                    $user_transaction2 = UserTransaction::create([
                        'user_id' => $user->id,
                        'transaction_id' => $transaction2->id,
                        'created_by' => $created_user->id,
                    ]);



                    $order_transaction  = OrderTransaction::create([
                        'order_id' => $order->id,
                        'transaction_id' => $transaction2->id,
                        'created_by' => $created_user->id,
                    ]);


                    if($order->amount==$order->transactionAmount($order->id))
                    {
                        $order->status->update([
                            'paid'      => 'payment_complated',
                        ]);

                    }else{
                        $order->status->update([
                            'paid'  => 'payment_paid',
                        ]);
                        return redirect()->route('admin.orders.show',['order'=>$order->id]);
                    }

                    return redirect()->route('admin.orders.my_orders.index');

                    }else
                    {
                        Session::flash('message', "Siz ".$amount." shuncha summa kiritishingiz kerak");
                        return back();
                    } 
                }
                else
                {
                    Session::flash('message', "Siz bu zakazga tulov qilib bulgansiz");
                    return back();
                }
                
            }


            $transaction1 = Transaction::create([
                'amount' => $request['payment'],
                'income' => false,
                'currency' => "UZB",
                'accepted' => true,
            ]);

            $transaction2 = Transaction::create([
                'amount' => $request['payment'],
                'income' => true,
                'currency' => "UZB",
                'accepted' => false,
            ]);


            $created_user->cash = $created_user->cash-$transaction1->amount;
            $created_user->update();

            $user->cash = $user->cash+$transaction2->amount;
            $user->update();

            $user_transaction1 = UserTransaction::create([
                'user_id' => $user->id,
                'transaction_id' => $transaction1->id,
                'created_by' => $created_user->id,
            ]);

            $user_transaction2 = UserTransaction::create([
                'user_id' => $user->id,
                'transaction_id' => $transaction2->id,
                'created_by' => $created_user->id,
            ]);



            $order_transaction  = OrderTransaction::create([
                'order_id' => $order->id,
                'transaction_id' => $transaction2->id,
                'created_by' => $created_user->id,
            ]);


            if($order->amount==$request['payment'])
            {
                $order->status->update([
                    'paid'      => 'payment_complated',
                ]);
            }else{
                $order->status->update([
                    'paid'  => 'payment_paid',
                ]);
            }

            return redirect()->route('admin.orders.show',$order->id);

        }
        else
        {
            $amount = $order->amount-$order->transactionAmount($order->id);
            Session::flash('message', "Siz ortiqcha pul kiritib yubordingiz ".$amount." siz shuncha kiriting");
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function user_payment(Request $request)
    {
        $user = User::find($request['user_id']);
        $created_user = User::find(auth()->id());

        $transaction = Transaction::create([
            'amount' => $request['payment'],
            'income' => true,
            'currency' => "UZB",
            'accepted' => true,
        ]);


        $user->cash = $user->cash+$transaction->amount;
        $user->update();

        $user_transaction = UserTransaction::create([
            'user_id' => $user->id,
            'transaction_id' => $transaction->id,
            'created_by' => $created_user->id,
        ]);

        return redirect()->route('admin.users.edit',$user->id);
    }

    public function user_check(Request $request)
    {   
        $transaction= Transaction::find($request['transaction_id']);
        $transaction->accepted=true;
        $transaction->update();
        return redirect()->route('admin.profil.account');
       
    }

    
}
