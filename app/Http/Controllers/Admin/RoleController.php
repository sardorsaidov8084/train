<?php

namespace App\Http\Controllers\Admin;
use App\Classes\Slug;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();

        return view('admin.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $this->validate($request,[
            'name' => 'required',
         ]);
        
         $params = [];
 
         $params['name'] = $request['name'];
         $params['level'] = Role::DEFAULT_LEVEL;//$request['name'];
         $params['description'] = $request['description'];
         $params['slug'] = Slug::createSlug($params['name'],Role::class);
 
         $role = Role::create($params);
 
         if ($role && isset($request['permissions'])) {
             $permissions = $request['permissions'];
             $role->permissions()->attach($permissions);
         }
 
        // Toastr::success('Добавлена роль');
         return redirect()->route('admin.roles.index');
 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($role)
    {  

        return view('admin.roles.show', compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($role)
    {
      

        return view('admin.roles.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $role)
    {
        $this->validate($request,[
            'name' => 'required'
        ]);
      
        $params = [];

        $params['name'] = $request['name'];
        $params['description'] = $request['description'];

        $role->update($params);

        if ($role && isset($request['permissions'])) {
            $permissions = $request['permissions'];
            $role->permissions()->sync($permissions);
        }

        //Toastr::success('Редактировать роль');
        return redirect()->route('admin.roles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($role)
    {
        $role->delete();
        return redirect()->route('admin.roles.index');
    }
}
