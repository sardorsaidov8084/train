<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Bank;
use App\Transaction;
use App\User;
use App\UserTransaction;
use App\OrderTransaction;
use App\Order;
use App\Role;
use DB;
use Session;
use App\BankUser;
class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = Order::find($request['order_id']);
        $created_user = User::find(auth()->id());
        $user = User::find($order->created_by);
        if($order->amount>=$request['payment'] && $created_user->cash>=$request['payment'])
        {
            $order_transaction_new = OrderTransaction::where(['order_id'=>$order->id])->get();
            if($order_transaction_new)
            {

                if($order->status->paid!="payment_complated")
                {

                    $amount = $order->amount-$order->transactionAmount($order->id);
                
                    if($amount>=$request['payment'])
                    {
                        // $transaction =  auth()->user()->createdAll();
                        // return count($transaction);

                    $transaction1 = Transaction::create([
                        'amount' => $request['payment'],
                        'income' => false,
                        'currency' => "UZB",
                        'accepted' => true,
                    ]);

                    $transaction2 = Transaction::create([
                        'amount' => $request['payment'],
                        'income' => true,
                        'currency' => "UZB",
                        'accepted' => false,
                    ]);

                    $user_transaction1 = UserTransaction::create([
                        'user_id' => $user->id,
                        'transaction_id' => $transaction1->id,
                        'created_by' => $created_user->id,
                    ]);

                    $user_transaction2 = UserTransaction::create([
                        'user_id' => $user->id,
                        'transaction_id' => $transaction2->id,
                        'created_by' => $created_user->id,
                    ]);



                    $order_transaction  = OrderTransaction::create([
                        'order_id' => $order->id,
                        'transaction_id' => $transaction2->id,
                        'created_by' => $created_user->id,
                    ]);

                    

                    // $bankUser = BankUser::where([
                    //         'user_id'=>$user->id,
                    //  ])->first();

                    //     if($bankUser){
                    //         $bankUser->amountall=$bankUser->amountall+$request['payment'];
                    //         $bankUser->update();
                    //     }else{
                    //         $bankUser = BankUser::create([
                    //         'user_id' => $user->id,
                    //         'created_by'=>$created_user->id,
                    //         'amountall'=>$request['payment'],
                    //     ]);
                    //     }

                    $bank = Bank::create([
                        'user_id'=>$user->id,
                        'created_by'=>$created_user->id,
                        'amount'=>$request['payment'],
                        'accepted'=>0,
                        'bank_user_id'=>0,
                    ]);
                    if($order->amount==$order->transactionAmount($order->id))
                    {
                        $order->status->update([
                            'paid'      => 'payment_complated',
                        ]);

                    }else{
                        $order->status->update([
                            'paid'  => 'payment_paid',
                        ]);
                        return redirect()->route('admin.orders.show',['order'=>$order->id]);
                    }



                    return redirect()->route('admin.orders.my_orders.index');

                    }else
                    {
                        Session::flash('message', "Siz ".$amount." shuncha summa kiritishingiz kerak");
                        return back();
                    } 
                }
                else
                {
                    Session::flash('message', "Siz bu zakazga tulov qilib bulgansiz");
                    return back();
                }
                
            }


            $transaction1 = Transaction::create([
                'amount' => $request['payment'],
                'income' => false,
                'currency' => "UZB",
                'accepted' => true,
            ]);

            $transaction2 = Transaction::create([
                'amount' => $request['payment'],
                'income' => true,
                'currency' => "UZB",
                'accepted' => false,
            ]);



            $user_transaction1 = UserTransaction::create([
                'user_id' => $user->id,
                'transaction_id' => $transaction1->id,
                'created_by' => $created_user->id,
            ]);

            $user_transaction2 = UserTransaction::create([
                'user_id' => $user->id,
                'transaction_id' => $transaction2->id,
                'created_by' => $created_user->id,
            ]);



            $order_transaction  = OrderTransaction::create([
                'order_id' => $order->id,
                'transaction_id' => $transaction2->id,
                'created_by' => $created_user->id,
            ]);


            if($order->amount==$request['payment'])
            {
                $order->status->update([
                    'paid'      => 'payment_complated',
                ]);
            }else{
                $order->status->update([
                    'paid'  => 'payment_paid',
                ]);
            }

            
            // $bankUser = BankUser::where([
            //     ['user_id'=>$user->id],
            //     ['created_by'=>$created_user->id],
            // ])->get()->first();

            // if($bankUser){
            //     return "pl";
            //     // $bankUser->amountall=$bankUser->amountall+$request['payment'];
            //     // $bankUser->update();
            // }else{
            //     $bankUser = BankUser::create([
            //     'user_id' => $user->id,
            //     'created_by'=>$created_user->id,
            //     'amountall'=>$request['payment'],
            // ]);
            // }
            $bank = Bank::create([
                'user_id'=>$user->id,
                'created_by'=>$created_user->id,
                'amount'=>$request['payment'],
                'accepted'=>0,
                'bank_user_id'=>0,
            ]);
            
            return redirect()->route('admin.orders.show',$order->id);
        }
        else
        {
            $amount = $order->amount-$order->transactionAmount($order->id);
            Session::flash('message', "Siz ortiqcha pul kiritib yubordingiz ".$amount." siz shuncha kiriting");
            return back();
        }

    }

    public function create_store(Request $request)
    {

        $created_user = User::find(auth()->id());
        if($created_user->cash<$request['payment'])
        {
            return back();
        }
        
        $created_user->cash = $created_user->cash-$request['payment'];
        $created_user->update();

        $user = User::find($request['user_id']);



        $bankUser = BankUser::where([
                'user_id'=>$created_user->id,
                'created_by'=>$user->id,

         ])->get()->first();

            if($bankUser){
                $bankUser->amountall=$bankUser->amountall+$request['payment'];
                $bankUser->update();
            }else{
                $bankUser = BankUser::create([
                    'user_id' => $created_user->id,
                    'created_by'=>$user->id,
                    'amountall'=>$request['payment'],
                ]);
            }
        $bank = Bank::create([
            'user_id'=>$user->id,
            'created_by'=>$created_user->id,
            'amount'=>$request['payment'],
            'accepted'=>0,
            'bank_user_id'=>$bankUser->id,
        ]);

        return redirect()->route('admin.profil.account');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function user_check(Request $request)
    {
        $bank = Bank::find($request['bank_id']);
        $bank->accepted=true;
        if($bank->bank_user_id!=0)
        {
            $bankUser = BankUser::where([
                'user_id'=>$bank->user_id,
                'created_by'=>$bank->created_by,
             ])->get()->first();

                if($bankUser){
                    $bankUser->amountall=$bankUser->amountall+$bank->amount;
                    $bankUser->update();
                }else{
                    $bankUser = BankUser::create([
                        'user_id' => $bank->user_id,
                        'created_by'=>$bank->created_by,
                        'amountall'=>$bank->amount,
                    ]);
                }
            $bank->bank_user_id=$bankUser->id;
            $bank->update();
        }
        

        return redirect()->route('admin.profil.account');
    }
}
