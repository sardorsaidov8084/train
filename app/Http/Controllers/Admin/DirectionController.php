<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Direction;
use App\StationTime;
use App\Station;
use App\StationDirection;
use App\TrainDirection;

class DirectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $directions = Direction::latest()->get();
        return view('admin.directions.index', compact('directions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        // $direction = Direction::where(['id' =>2])->first();
        // dd($direction->stations);
        return view('admin.directions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
       
        $this->validate($request,[
            'name' => 'required|unique:directions',
            'train_id' => 'required',
        ]);
        $direction = Direction::create([
            'name' => $request['name'],
        ]);
        TrainDirection::create([
            'train_id' => $request['train_id'],
            'direction_id' => $direction->id,
        ]);
        $train_id = $request['train_id'];
        $station_from =0;
        if($request['station']){
            foreach ($request['station'] as $value) {
                StationDirection::create([
                    'station_id' => $value['id'],
                    'direction_id' => $direction->id,
                ]);
                StationTime::create([
                    'station_from' =>  $station_from,
                    'station_to' => $value['id'],
                    'time' => $value['day'],
                    'direction_id' => $direction->id,
                    'train_id' => $train_id,
                ]);
                $station_from = $value['id'];
            }
        }
        return redirect()->route('admin.directions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($direction)
    {
       
        return view('admin.directions.edit', compact('direction'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $direction)
    {
       
     
        $this->validate($request,[
            'name' => 'required',
            'train_id' => 'required',
        ]);
        Direction::where(['id' =>$direction->id])->update([
            'name' => $request['name'],
        ]);
        TrainDirection::where(['direction_id' =>$direction->id])->update([
            'train_id' => $request['train_id'],
            'direction_id' => $direction->id,
        ]);
         $stationTime =StationTime::where(['direction_id' => $direction->id])->delete();
         
        $station_from =0;
        if($request['station']){
            foreach ($request['station'] as $value) {
               
                // StationDirection::create([
                //     'station_id' => $value['id'],
                //     'direction_id' => $direction->id,
                // ]);
                StationTime::create([
                    'station_from' =>  $station_from,
                    'station_to' => $value['id'],
                    'time' => $value['day'],
                    'direction_id' => $direction->id,
                ]);
                $station_from = $value['id'];
               
            }
        }
        return redirect()->route('admin.directions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($direction)
    {
        $direction->delete();
        return redirect()->route('admin.directions.index');
    }
}
