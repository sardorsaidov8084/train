<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Company;
use App\Image;
use App\Product;
use App\User;
use Response;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::latest()->get();
        
        return view('admin.companies.index',compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'description' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if ($request->hasFile('image'))
        {
            $file = $request->image;
            $image = Image::store($file,'company',null,null);
            if (!is_null($image))
            {
               $image_id = $image['id'];
            }
        }

       $company = Company::create([
            'name'        => $request['name'],
            'description' => $request['description'],
            'address'     => $request['address'],
            'phone'       => $request['phone'],
            'image_id'    => $image_id,
       ]);
       return redirect()->route('admin.companies.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::find($id);
        $users = User::where(['company_id'=>$company->id])->get();
        $products = Product::where(['company_id'=>$company->id])->get();
        return view('admin.companies.show',compact('company','users','products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        return view('admin.companies.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $company)
    {
        $this->validate($request,[
            'name' => 'required',
            'description' => 'required',
            'address' => 'required',
            'phone' => 'required',
        ]);
        Company::where(['id' => $company])->update([
            'name' => $request['name'],
            'description' => $request['description'],
            'address' => $request['address'],
            'phone' => $request['phone'],
        ]);

        return redirect()->route('admin.companies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
        $company->delete();
        return redirect()->route('admin.companies.index');
    }
    public function companySelect(){
        $companies = Company::get();
        
        return Response::json($companies);
    }
}
