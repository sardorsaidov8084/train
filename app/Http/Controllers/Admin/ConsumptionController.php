<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use App\Consumption;

class ConsumptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $consumptions = Consumption::get();
       
        return view('admin.consumptions.index', compact('consumptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.consumptions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,[
          'title' => 'required',
          'amount' => 'required',
      ]);
     
      $consumptions = Consumption::create([
          'title' => $request['title'],
          'description' => $request['description'],
          'amount' => $request['amount'],
      ]);
      Toastr::success('расход добавленной');
      return redirect()->route('admin.consumptions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $consumption = Consumption::find($id);
        return view('admin.consumptions.edit', compact('consumption'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required',
            'amount' => 'required',
        ]);
        $consumption = Consumption::find($id);
        $consumption ->update($request->all());
        
        Toastr::success('расход обновленный');;
        return redirect()->route('admin.consumptions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $consumption = Consumption::find($id);
        $consumption->delete();
        return redirect()->route('admin.consumptions.index');
    }
}
