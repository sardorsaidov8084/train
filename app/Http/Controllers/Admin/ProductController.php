<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Image;
use App\Role;
use App\Category;
use App\Company;
use Response;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::latest()->get();
        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // if(!auth()->user()->hasRole(Role::DOSTAVKA))
        // {
        //     return redirect()->route('admin.products.index');
        // }
        $categories = Category::latest()->get();
        $companies = Company::latest()->get();
        return view('admin.products.create',compact('categories','companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
       $this->validate($request,[
            'name'        =>  'required',
            'category_id' =>  'required',
            'company_id'  =>  'required',
            'price'       =>  'required',
            'quantity'    =>  'required',
            'image'       =>  'image|mimes:jpeg,png,jpg,gif,svg|max:4096',
            'weight'      => 'required'

       ]);
        $image_id = null;
       if ($request->hasFile('image'))
       {
           $file = $request->image;
           $image = Image::store($file,'product',null);
           if (!is_null($image))
           {
               $image_id = $image['id'];
           }
       }
      
       $product = Product::create([
            'name'        => $request['name'],
            'category_id' => $request['category_id'],
            'company_id'  => $request['company_id'],
            'price'       => $request['price'],
            'quantity'    => $request['quantity'],
            'description' => $request['description'],
            'status'      => (isset($request['status']))?Product::STATUS:Product::NO_STATUS,
            'image_id'    => $image_id,
            'weight'      => $request['weight'],
       ]);
       return redirect()->route('admin.products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($product)
    {  
        // if(!auth()->user()->hasRole(Role::DOSTAVKA))
        // {
        //     return redirect()->route('admin.products.index');
        // }
        $categories = Category::latest()->get();
        $companies = Company::latest()->get();
        return view('admin.products.edit', compact('product','categories','companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $product)
    {   

         $this->validate($request,[
            'name'        =>  'required',
            'category_id' =>  'required',
            'company_id'  =>  'required',
            'price'       =>  'required',
            'quantity'    =>  'required',
            'image'       =>  'image|mimes:jpeg,png,jpg,gif,svg|max:4096'

       ]);
        
         
        $image_id = null;
            if ($request->hasFile('image'))
            {
               $file = $request->image;
               $image = Image::store($file,'product',$product->image_id);
               if (!is_null($image))
               {
                   $image_id = $image['id'];
               }
            }
            else{
                $image_id = $product->image_id;
            }

       //
        Product::where(['id' => $product->id])->update([
            'name'        => $request['name'],
            'category_id' => $request['category_id'],
            'company_id'  => $request['company_id'],
            'price'       => $request['price'],
            'quantity'    => $request['quantity'],
            'description' => $request['description'],
            'image_id'    => $image_id,
            'weight'      => $request['weight'],
       ]);
       return redirect()->route('admin.products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($product)
    {
        $image = Image::find($product->image_id);
        if($image){
            $image->delete_file($image->path);
            $image->delete();
        }
        $product->delete();
        return redirect()->route('admin.products.index');                                        
    }

    public function productSelect(){
        $products = Product::get();
        return Response::json($products);
    }
    public function companyProduct($id = null){
        
        if($id != null){
            $products= Product::where(['company_id' =>$id])->get();
            
                return Response::json($products);
          
           
        }
        else{
            return Response::json(false);
        }
    }
}
