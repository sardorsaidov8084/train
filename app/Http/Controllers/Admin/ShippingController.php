<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use App\Http\Controllers\Controller;
use App\Order;
use App\Product;
use App\Shipping;
use App\Role;
use App\OrderProduct;
use App\OrderStatus;
use App\Status;

class ShippingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        $arrayErrorName = []; 
        $order_products =OrderProduct::where(['order_id' => $request['order_id']])->get();
        foreach($order_products as $order_product){
            $product = Product::find($order_product->product_id);
            $product->quantity =  $product->quantity - $order_product->quantity;
            if( $product->quantity<0){
                array_push($arrayErrorName,$product->name);
            }
        }
        if(count($arrayErrorName) > 0){
            Toastr::error('На складе не хватает продуктов!');
            return redirect()->back();
        }

        $shipping = new Shipping;
        $shipping->user_id = auth()->id();
        $shipping->order_id=$request->order_id;
        $shipping->from_country=$request->from_country;
        $shipping->to_country=$request->to_country;
        $shipping->from_station=$request->from_station;
        $shipping->to_station=$request->to_station;
        $shipping->direction_id=$request->direction_id;
        $shipping->train_id=$request->train_id;
        $shipping->wagon_id=$request->wagon_id;
        $shipping->container_id=$request->container_id;
        $shipping->from_date=date('Y-m-d h:m',strtotime($request['from_date']));
        $shipping->to_date=date('Y-m-d h:m',strtotime($request['to_date']));
        $shipping->save();
        
        $order = Order::find($request['order_id']);
        $order_products =OrderProduct::where(['order_id' => $request['order_id']])->get();
        foreach($order_products as $order_product){
            $product = Product::find($order_product->product_id);
            $product->quantity =  $product->quantity - $order_product->quantity;
            $product->update();
        }
        $status = Status::find(5);
        $order->status->update([
            'body' => $status->name,
        ]);
        $order->update(); 
        if(auth()->user()->hasRole(Role::DOSTAVKA)){
            $orders = Order::whereIn('id',OrderStatus::status(OrderStatus::AGENT)
                ->pluck('order_id')->toArray())->latest('created_at')->get();   

            return redirect()->route('admin.orders.index',compact('orders'));  
        }
      
        if(auth()->user()->hasRole(Role::AGENT)){
            
            $orders = Order::where('user_id', '!=', auth()->id())->get();
            
        }
        Toastr::success('Успешно!');
        return redirect()->route('admin.orders.my_orders.index',compact('orders'));
        }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        
    }


    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    

}
