<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Wagon;
use App\Train;

class WagonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wagons = Wagon::latest()->get();

        return view('admin.wagons.index', compact('wagons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.wagons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'train_id' => 'required',
        ]);
        Wagon::create([
            'name' => $request['name'],
            'train_id' => $request['train_id'],
        ]);
        return redirect()->route('admin.wagons.index');         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($wagon)
    {   
        return view('admin.wagons.edit',compact('wagon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $wagon)
    {
        
        $this-> validate($request,[
            'train_id' => 'required',
        ]);
        Wagon::where(['id' => $wagon->id])->update([
            'name' => $request['name'],
            'train_id' => $request['train_id'],
        ]);
        return redirect()->route('admin.wagons.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($wagon)
    {
        $wagon->delete();
        return redirect()->route('admin.wagons.index');
    }
}
