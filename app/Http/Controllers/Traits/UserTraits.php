<?php
/**
 * Created by PhpStorm.
 * User: Good One Sales
 * Date: 10/28/2018
 * Time: 9:16 PM
 */

namespace App\Http\Controllers\Traits;


trait UserTraits
{
    protected $roles;

    protected $permissions;

    public function do($permission){

        if (auth()->user()->hasPermission($permission))
            return true;

        foreach (auth()->user()->getRoles() as $role) {
            if ($role->hasPermission($permission))
                return true;
        }
        return false;
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role','role_user');
    }

    public function getRoles()
    {
        return (!$this->roles) ? $this->roles = $this->roles()->get() : $this->roles;
    }

    public function getRole($roleName = null)
    {
        return \App\Role::where('slug',$roleName)->first();
    }

    public function hasRole($role)
    {
        return $this->getRoles()->contains($this->getRole($role));
    }

    public function attachRole($role)
    {
        return (!$this->getRoles()->contains($role)) ? $this->roles()->attach($role) : true;
    }

    public function detachRole($role)
    {
        $this->roles = null;
        return $this->roles()->detach($role);
    }

    public function detachAllRoles()
    {
        $this->roles = null;
        return $this->roles()->detach();
    }

    public function level()
    {
        return ($role = $this->getRoles()->sortByDesc('level')->first()) ? $role->level : 0;
    }

    public function permissions()
    {
        return $this->belongsToMany('App\Permission','permission_user');
    }

    public function getPermissions()
    {
        return (!$this->permissions) ? $this->permissions()->get() : $this->permissions;
    }

    public function getPermission($permissionName = null)
    {
        return \App\Permission::where('slug',$permissionName)->first();
    }

    public function hasPermission($permission)
    {
        return $this->getPermissions()->contains($this->getPermission($permission));

    }

    public function attachPermission($permission)
    {
        return (!$this->getPermissions()->contains($permission)) ? $this->permissions()->attach($permission) : true;
    }

    public function detachPermission($permission)
    {
        $this->permissions = null;
        $this->permissions()->detach($permission);
    }

    public function detachAllPermissions()
    {
        $this->permissions = null;
        $this->permissions()->detach();
    }

    public function verified(){
        return ($this->hasOne('App\VerifyUser','user_id')->first() && $this->hasOne('App\VerifyUser','user_id')->first()->verified == 1) ? true : false;
    }

    public function banned(){
        return ($this->hasOne('App\BanUser','user_id')->first()) ? true : false;
    }

    public function profile(){
        return $this->hasOne('App\Profile');
    }

}
