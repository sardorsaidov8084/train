<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $fillable = [
        'order_id',
        'product_id',
        'company_id',
        'product_price',
        'quantity',
        'amount',
        'weight',
    ];
    public function products(){

        return $this->belongsTo('App\Product','product_id');
    }
    public function companies(){
        return $this->belongsTo('App\Company','company_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Company','company_id','id');
    }
   
}
