<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wagon extends Model
{
    protected $fillable = [
        'name',
        'train_id',
    ];
    public function train(){

        return $this->belongsTo('App\Train');
    }
    public function containers(){

        return $this->belongsTo('App\Container');
    }
}
