<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
	protected $fillable =[
        'user_id',
        'order_id',
        'from_country',
        'to_country',
        'from_station',
        'to_station',
        'direction_id',
        'train_id',
        'wagon_id',
        'container_id',
        'from_date',
        'to_date'
    ];
    public function status(){
        return $this->belongsTo(ShippingStatus::class,'id','shipping_id')->withoutGlobalScope('state');
    }
    public function country()
    {
        return $this->belongsTo('App\Country','id');
    }
    public function station()
    {
        return $this->belongsTo('App\Station');
    }
    public function direction()
    {
        return $this->belongsTo('App\Direction');
    }
    public function train()
    {
        return $this->belongsTo('App\Train');
    }
    public function wagon()
    {
        return $this->belongsTo('App\Wagon');
    }
    public function container()
    {
        return $this->belongsTo('App\Container');
    }

}
