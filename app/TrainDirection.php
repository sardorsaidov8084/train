<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainDirection extends Model
{
    protected $fillable =['train_id', 'direction_id','start_time', 'finish_time'];
}
