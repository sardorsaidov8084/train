<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quantity extends Model
{
    protected $fillable = ['product_id', 'company_id','cost'];

    public function products(){

        return $this->belongsTo('App\Product');
    }
}

