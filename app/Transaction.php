<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Transaction;
use App\Order;
use App\OrderTransaction;

class Transaction extends Model
{
    protected $fillable = [
    	'amount',
    	'income',
    	'currency',
    	'accepted',
    ];

    public function users(){

        return $this->belongsTo('App\UserTransaction','id','transaction_id');
    }
    public function allAmount($transaction_id)
    {
    	$transactions = Transaction::where(['id'=>$transaction_id])->get();
    	$amount=0;
    	foreach ($transactions as $transaction) {
    		$amount+=$transaction;
    	}
    	return $amount;
    }

    public function orderID($id)
    {
    	$orderTransaction = OrderTransaction::where(['transaction_id'=>$id])->value('order_id');
    	if(!$orderTransaction)
    	{
    		$orderTransaction = OrderTransaction::where(['transaction_id'=>$id+1])->value('order_id');
    	}
    	// $order = Order::where(['id'=>$orderTransaction->order_id])->get();
    	return $orderTransaction;
    }

}
