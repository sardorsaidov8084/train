<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Container extends Model
{
    protected $fillable = [
        'name',
        'wagon_id',
        'image_id',
        'order_id',
        
    ];
    public function image(){

        return $this->belongsTo('App\Image');
    }
   
}
