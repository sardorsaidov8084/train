<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'description',
        'image_id',
        'status',
        'category_id',
        'company_id',
        'price',
        'quantity',
        'weight',
        
    ];
    const STATUS = 1;
    const NO_STATUS = 0;

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function company(){
        return $this->belongsTo('App\Company');
    }
    
    public function image(){
        return $this->belongsTo('App\Image');
    }
    
}
