<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
//use Schema;
use Auth;
class AppServiceProvider extends ServiceProvider
{
   
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

   
    public function register()
    {
        Blade::directive('permission', function ($permission) {
            return "<?php if (auth()->user()->do({$permission})): ?>";
        });

        Blade::directive('endpermission', function () {
            return '<?php endif ?>';
        });
    }
}