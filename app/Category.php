<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name',
        'description',
        'image_id',
        'status',
        'parent_id'
    ];
const STATUS = 1;
const NO_STATUS = 0;
/**
 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
 */
public function image(){

    return $this->belongsTo('App\Image');
}
}
