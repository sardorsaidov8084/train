<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StationDirection extends Model
{        
    protected $fillable = ['station_id', 'direction_id'];

    public $timestamps =false;
}
