<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StationTime extends Model
{
    protected $fillable =['station_from', 'station_to', 'direction_id','time','train_id'];
    public $timestamps = null;
}
