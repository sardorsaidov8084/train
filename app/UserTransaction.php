<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTransaction extends Model
{
    protected $fillable = [
    	'user_id',
    	'transaction_id',
    	'created_by',
    ];

    public function receiver()
    {
    	return $this->belongsTo('App\User','user_id','id');
    }

    public function creater()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }

    public function transaction()
    {
        return $this->belongsTo('App\Transaction','transaction_id','id');
    }

}
