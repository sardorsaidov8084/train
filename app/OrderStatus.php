<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
class OrderStatus extends Model
{
    const STATUES = [
        'pokupatel'  => 'Покупатель',
        'my_orders'   => 'Мои заказы',
        'agent' => 'Посредник',
        'create_orders' => 'Мои заказы',
        
    ];
   
    /**
     *
     */
    const MY_ORDERS =  'my_orders';
    /**
     *
     */
    const AGENT = 'agent';
    const POKUPATEL ='pokupatel';

    protected $fillable = [
        'status',
        'order_id',
        'delivery',
        'state',
        'paid',
        'body',
        'agent',
    ];
    const STATE_CREATED = 'created';
    const STATE_CANCELED = 'canceled';
    const STATE_COMPLETED = 'completed';
    const STATE_AGENT = 'created';

    public function order(){
        return $this->belongsTo('App\Order');
    }

    public function scopeStatus($query,$status)
    {
        return $query->where('status',$status);
    }
    public function scopeDelivery($query,$status)
    {
        return $query->where('delivery',OrderStatus::STATE_CANCELED);        
    }
    public function scopePaid($query,$status)
    {
        return $query->where('paid',OrderStatus::STATE_COMPLETED);        
    }
    public function scopeState($query,$state)
    {
        return $query->where('state',OrderStatus::STATE_CREATED);        
    }
    public function scopeAgent($query,$state)
    {
        return $query->where('agent',OrderStatus:: STATE_AGENT);        
    }
}
