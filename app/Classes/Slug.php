<?php
/**
 * Created by PhpStorm.
 * User: Azizbek Eshonaliyev
 * Date: 10/28/2018
 * Time: 4:00 PM
 */

namespace App\Classes;


use Illuminate\Support\Facades\Log;

class Slug
{
    /**
     * @param $title
     * @param int $id
     * @return string
     * @throws \Exception
     */
    public static function createSlug($title, $model_type = null)
    {   
        $slug = str_slug($title);

        $allSlugs = Slug::getRelatedSlugs($slug, $model_type);

        if (is_null($allSlugs)){
            
            return $slug;
        }

        for ($i = 1; $i <= 100; $i++) {
            $newSlug = $slug.'-'.$i;
            $allSlugs = Slug::getRelatedSlugs($newSlug,$model_type);
            if (is_null($allSlugs)){
                return $newSlug;
            }
        }
        throw new \Exception('Can not create a unique slug');
    }
    protected static function getRelatedSlugs($slug, $model_type)
    {
        return (($model_type)::where('slug',$slug)->first());
    }
}
