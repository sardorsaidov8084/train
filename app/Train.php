<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Train extends Model
{
    protected $fillable = [
        'name',
        'description',
       
    ];
    public function wagons(){

        return $this->belongsTo('App\Wagon');
    }
}
