<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingStatus extends Model
{
    const STATUES = [
        'pokupatel'  => 'Покупатель',
        'dostavka' => 'Доставка ',
        'my_orders'   => 'Мои заказы',
        'agent' => 'Посредник',
        'create_orders' => 'Мои заказы',
        
    ];
   
    /**
     *
     */
    const MY_ORDERS =  'my_orders';
    /**
     *
     */
    const DOSTAVKA = 'dostavka';
    const AGENT = 'agent';
    const POKUPATEL ='pokupatel';

    protected $fillable = [
        'status',
        'shipping_id',
        'delivery',
        'state',
        'paid',
        'body',
    ];
    const STATE_CREATED = 'created';
    const STATE_CANCELED = 'canceled';
    const STATE_COMPLETED = 'completed';

    public function shipping(){
        return $this->belongsTo('App\Shipping');
    }

    public function scopeStatus($query,$status)
    {
        return $query->where('status',$status);
    }
    public function scopeDelivery($query,$status)
    {
        return $query->where('delivery',$status);        
    }
    public function scopePaid($query,$status)
    {
        return $query->where('paid',$status);        
    }
    public function scopeState()
    {
        return $query->where('state',ShippingStatus::STATE_CREATED);        
    }
}
