<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\StationDirection;

class Direction extends Model
{
    protected $fillable = [
        'name',
        
    ];
    public function stations(){
        return $this->belongsToMany('App\Station','station_directions');
    }
    public function trains(){
        return $this->belongsToMany('App\Train','train_directions');
    }
}
