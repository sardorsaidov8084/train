<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankUser extends Model
{
    //

    protected $fillable = [
    	'user_id',
    	'created_by',
    	'amountall',
    ];
    
    public function receiver()
    {
    	return $this->belongsTo('App\User','user_id','id');
    }

    public function creater()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
}
