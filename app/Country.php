<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [
        'name',
        
    ];

    public function shipping()
    {
    	return $this->belongsTo('App\Shipping');
    }
    public function stations(){
        return $this->belongsTo('App\Station');
    }
}
