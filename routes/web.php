<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('admin/login/', function () {
    $role = App\Role::where(['slug'=>'agent'])->first();
    $user_id = DB::table('role_user')->where('role_id',$role->id)->value('user_id');
    $phone = App\User::where(['id'=>$user_id])->value('phone');
    return view('admin.login',compact('phone'));
})->name('user.login');
Route::get('admin/register/', function () {
    return view('admin.register');
});
Route::get('user/contact',function(){
    return view('contact.create');
});

    Route::resource('contacts','ContactController');

Route::post('admin/login','Admin\AdminController@login')->name('admin.login');
Route::post('admin/register','Admin\AdminController@register')->name('admin.register');
Route::get('admin/logout','Admin\AdminController@logout')->name('admin.logout'); 
Route::get('/stations/select', 'Admin\StationController@select')->name('stations.select');

Route::middleware('admin')->prefix('admin')->name('admin.')->namespace('Admin')->group(function()
{
    Route::get('index/', 'AdminController@index')->name('admin.index');
    Route::get('/user/role/{id}','UserController@createRole')->name('users.role');
    Route::get('/user/role/attach/{user}/{role}','UserController@storeRole')->name('users.role_attach');
    Route::get('/user/role/detach/{user}/{role}','UserController@detachRole')->name('users.role_detach');

    Route::get('orders/my_orders/index', 'OrderController@myOrdersIndex')->name('orders.my_orders.index');
    Route::post('orders/my_orders/accept', 'OrderController@myOrdersAccept')->name('orders.my_orders.accept');
    Route::post('orders/my_orders/confirm', 'OrderController@myOrdersConfirm')->name('orders.my_orders.confirm');
    Route::post('orders/my_orders/delete', 'OrderController@myOrdersDelete')->name('orders.my_orders.delete');
    Route::post('orders/my_orders/wait', 'OrderController@myOrdersWait')->name('orders.my_orders.wait');
    Route::post('orders/my_orders/ready', 'OrderController@myOrdersReady')->name('orders.my_orders.ready');
    Route::get('orders/canceleds/index', 'OrderController@canceleds')->name('orders.canceleds.index');
    Route::get('orders/closed/index', 'OrderController@closed')->name('orders.closed.index');

    Route::post('orders/complete','OrderController@orderComplete')->name('orders.complete');
    Route::get('orders/order_from_station/{id?}', 'OrderController@orderFromStation');
    Route::get('orders/order_train/{id?}', 'OrderController@orderTrain');
    Route::get('orders/order_wagon/{id?}', 'OrderController@orderWagon');
    Route::get('orders/order_container/{id?}', 'OrderController@orderContainer');
    Route::get('/orders/orderPrice/{id?}','OrderController@orderPrice')->name('orderPrice');
    Route::get('/products/productSelect/','ProductController@productSelect')->name('productSelect');

    Route::get('/companies/companySelect/','CompanyController@companySelect')->name('companySelect');
    Route::get('/products/company_product/{id?}','ProductController@companyProduct')->name('products.company_product');

    Route::get('/profil/account','UserController@account')->name('profil.account');
    Route::get('stores','StoreController@index')->name('stores.index');
    Route::post('stores/add','StoreController@add')->name('stores.add');
    Route::post('stores/getOut','StoreController@getOut')->name('stores.getOut');

    Route::post('/users/user_payment','TransactionController@user_payment')->name('transactions.user_payment');

    Route::post('/profil/user_check','TransactionController@user_check')->name('transactions.user_check');

    Route::post('banks/store','BankController@store')->name('banks.store');
    Route::post('banks/create/store','BankController@create_store')->name('banks.create.store');
    Route::post('banks/user_check','BankController@user_check')->name('banks.user_check');

    Route::resource('users', 'UserController');
    Route::resource('categories', 'CategoryController' );
    Route::resource('products', 'ProductController' );
    Route::resource('trains', 'TrainController' );
    Route::resource('countries', 'CountryController' );
    Route::resource('wagons', 'WagonController' );
    Route::resource('containers', 'ContainerController' );
    Route::resource('stations', 'StationController' );
    Route::resource('directions', 'DirectionController' );
    Route::resource('orders', 'OrderController');
    Route::resource('permissions', 'PermissionController');
    Route::resource('roles', 'RoleController');
    Route::resource('shippings','ShippingController');
    Route::resource('status','StatusController');
    Route::resource('companies','CompanyController');
    Route::resource('transactions','TransactionController');
    Route::resource('consumptions','ConsumptionController');
   


});
