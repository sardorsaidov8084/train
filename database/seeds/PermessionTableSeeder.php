<?php

use Illuminate\Database\Seeder;

class PermessionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Schema::hasTable('permissions')) {
            
            \App\Permission::firstOrCreate([
                    'name'          => 'Admin Views',
                    'description'   => 'Admin Views',
                    'slug'          => 'show.admin.views',
            ]);
            /*
             * Order
             */
            \App\Permission::firstOrCreate([
                'name'  => 'Create order',
                'description'  => 'Create order',
                'slug'  => 'create.orders',
            ]);
        }    
    }
}
