<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStationTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('station_times', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('direction_id');
            $table->integer('train_id');
            $table->integer('station_from');
            $table->integer('station_to');
            $table->string('time');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('station_times');
    }
}
