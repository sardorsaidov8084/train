<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shippings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id');
            $table->integer('user_id');
            $table->integer('from_country')->unsigned();
            $table->integer('to_country')->unsigned();
            $table->integer('from_station')->unsigned();
            $table->integer('to_station')->unsigned();
            $table->integer('direction_id')->unsigned();
            $table->integer('train_id')->unsigned();
            $table->integer('wagon_id')->unsigned();
            $table->integer('container_id')->unsigned();
            $table->datetime('from_date');
            $table->datetime('to_date');
            $table->string('from_time')->nullable();
            $table->string('to_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shippings');
    }
}
