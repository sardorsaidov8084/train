
@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/lib/chosen/chosen.min.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <style>
    .chosen-container{
        margin-left:15px;
    }
    </style>
   
@endsection
@section('content')
<div id="right-panel" class="right-panel">

  	@if(auth()->user())
  		@include('admin.includes.header')
  	@endif

    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Форма элементов</strong> 
                        </div>
                        <div class="card-body">
                            <div class="card-body card-block">
                                <form action="{{ route('contacts.store')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="name" class=" form-control-label">Имя</label>
                                                </div>
                                                <div class="col-12 col-sm-9">
                                                    <input type="text" id="name" name="name" placeholder="" class="form-control">
                                                    @if($errors->has('name'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('name') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="name" class=" form-control-label">Телефон</label>
                                                </div>
                                                <div class="col-12 col-sm-9">
                                                    <input type="number" id="phone" name="phone" placeholder="(998991112233)" class="form-control">
                                                    @if($errors->has('phone'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('phone') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="title" class=" form-control-label">Заглавие</label>
                                                </div>
                                                <div class="col-12 col-sm-9">
                                                    <input type="text" id="title" name="title" placeholder="" class="form-control">
                                                    @if($errors->has('title'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('title') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            
                                        </div>
                                       
                                       
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="body" class=" form-control-label">Описание</label>
                                                </div>
                                                <div class="col-12 col-sm-9">
                                                    <textarea type="text" id="body" name="body" rows="4" placeholder="описание" class="form-control"></textarea>
                                                    @if($errors->has('body'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('body') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>  
                                        
                                    </div>
                                    <br>
                                    <div class="row form-group">
                                        <div class="col-12 col-sm-9">
                                            <button type="submit" class="btn btn-primary">Отправить</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>                     
                </div>
            </div>
            
        </div><!-- .animated -->
    </div><!-- .content -->

    <div class="clearfix"></div>

</div><!-- /#right-panel -->

@endsection
@section('script')
<script src="{{ asset('admin/js/lib/chosen/chosen.jquery.min.js')}}"></script>

<script>
    jQuery(document).ready(function() {
        jQuery(".standardSelect").chosen({
            disable_search_threshold: 10,
            no_results_text: "Oops,Ничего не найдено!",
            width: "100%"
        });
    });
</script>
@endsection
