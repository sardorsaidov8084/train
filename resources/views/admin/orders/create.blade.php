@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/lib/chosen/chosen.min.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <style>
        .chosen-container{
            margin-left:15px;
            width:83% !important;
        }
    </style>
   
@endsection
@section('content')
<div id="right-panel" class="right-panel">
        
  @include('admin.includes.header')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-5">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><strong><a href="{{route('admin.orders.my_orders.index')}}">Мои заказы</a></strong></li>
                                <li class="">Добавить </li>
                            </ol>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Форма элементов</strong> 
                        </div>
                        <div class="card-body">
                            <div class="card-body card-block">
                                <form action="{{ route('admin.orders.store')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="address" class=" form-control-label">Aдрес</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <textarea type="text" id="address" name="address" rows="2" placeholder="адрес" class="form-control" required></textarea>
                                                  
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="address" class=" form-control-label"> Выберите компанию</label>
                                                </div>
                                                <div class="col-12 col-sm-9">
                                                    <select name="company_id" id="company_id" class="form-control" >
                                                    @foreach(App\Company::get() as $company)
                                                        <option value="{{$company->id}}">{{$company->name}}</option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="category_attribut_rows">
                                        
                                     </div>
                                     <div class="row">
                                        <div class=" form-group">
                                                        
                                            <div class="col-12 col-sm-9">
                                                <span id="addAttribute" class="btn btn-info" >Добавить Продукты</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row form-group" style="float:right">
                                        <div class="col-12 col-sm-9">
                                            <button type="submit" class="btn btn-primary">Добавить</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>                     
                </div>
            </div>
            
        </div><!-- .animated -->
    </div><!-- .content -->

    <div class="clearfix"></div>

</div><!-- /#right-panel -->

@endsection
@section('script')
<script src="{{ asset('admin/js/lib/chosen/chosen.jquery.min.js')}}"></script>
<script>
    
    let counter = 0;

    jQuery(document).ready(function() {
        jQuery(".standardSelect").chosen({
            disable_search_threshold: 10,
            no_results_text: "Oops, nothing found!",
            width: "100%"
        });


    });
    let old = null;
    jQuery("#company_id").on('change',function(event) {
        event.preventDefault();
        if(old != jQuery(this).val()) 
            jQuery("#category_attribut_rows").html('');
            old = jQuery(this).val();
    });
    let companiesGet = function(callback){
        var data;
        jQuery.ajax({
                    url: "{{ route('admin.companySelect')}}",
                    type:'get',
                    async: false,
         success: function(companies){
            text = "";        
             text = text +'<option value >' + '</option>';
             for(var i =0; i < companies.length; i++){
               
                 text = text + '<option  data-p_id=\"'+companies[i].id+'\" value=\"' + companies[i].id + '\">' + companies[i].name + '</option>';
             }   
             data = text;
  
        }});
        return data;
    }
    let stationsGet = function(callback){
        var data;
        jQuery.ajax({
                    url: "/admin/products/productSelect/",
                    type:'get',
                    async: false,
         success: function(results){
            text = "";        
             text = text +'<option value >' + '</option>';
             for(var i =0; i < results.length; i++){
               
                 text = text + '<option  data-p_id=\"'+results[i].id+'\" value=\"' + results[i].id + '\">' + results[i].name + '</option>';
             }   
             data = text;
  
        }});
        return data;
    }
    function  reloadCompany(counter){
        let val =jQuery("#company_id").val();
         
           var url = '/admin/products/company_product/' + val;
            jQuery.ajax({
            url: url,
            type: 'get',
            success: function (data) {
                products = data;
                text = "";        
                text = '<option> Выберите продукт...</option>';
                for(var i =0; i <products.length; i++){
                    text = text + '<option value=\"' +products[i].id + '\">' +products[i].name + '</option>';
                    
                }  
               
                jQuery("#product-"+counter).html(text);
               
            }
        });
        
    }
    jQuery("#addAttribute").click(function () {
                           
                counter++;                
                 
                    let row = "<div class =\"row\" id=\""+counter+"\" style=\"margin-bottom:20px !important;\">\n"+
                               
                     "<div class=\"col-md-4 \">\n" +
                        "<div class=\" form-group\">"+
                        "<select data-counter=\""+counter+"\" placeholder=\"Продукты\" name=\"product["+counter+"][id]\" required class=\"form-control\" id=\"product-"+counter+"\"  tabindex=\"1\" onchange=\"changeInputTypeBySelect(this)\">\n" +
                          stationsGet()
                       
                        + "</select>\n" +
                        "</div>"+
                    "</div>\n" +
                    "<div class=\"col-md-3\">\n" +
                        "<input class=\"form-control\" type=\"number\" name=\"product["+counter+"][price]\" id=\"price-"+counter+"\"   required placeholder=\"Цена (1 кг/т)\">\n" +
                    "</div>\n" +
                    "<div class=\"col-md-1\">\n" +
                        "<select required class=\"form-control\"  id=\"weight-"+counter+"\" name=\"product["+counter+"][weight]\" tabindex=\"1\">\n" +
                           
                         "</select>\n" +
                    "</div>\n" +
                    "<div class=\"col-md-2\">\n" +
                     "<input class=\"form-control\" type=\"number\" name=\"product["+counter+"][quantity]\" id=\"quantity\" required placeholder=\"\">\n" +
                    "</div>\n" +
                    "<div col-md-1>"+
                        "<span onclick='removeThis(this)' class=\"delete btn btn-danger \" data-id="+counter+"><i class=\"fa fa-trash\"></i></span>\n" +
                    "</div>"+
                "</div>\n";
                
                jQuery('#category_attribut_rows').append(row);
                reloadCompany(counter);
                
            });

            function removeThis(e) {
                jQuery("#" + jQuery(e).data('id')).remove();
               
            }
            
            function changeInputTypeBySelect(e){
             let price_counter = jQuery(e).data('counter');   
             let val =jQuery(e).val();
             var url = '/admin/orders/orderPrice/' + val;
            jQuery.ajax({
            url: url,
            type: 'get',
            success: function (data) {
               $data = data; 
                jQuery("#price-"+price_counter).val(data.price);
                weight = '';
               
                weight =weight + '<option value=\"' +data.weight + '\">' +data.weight + '</option>';
               jQuery("#weight-"+price_counter).html(weight); 
            }
        });
                let $element =jQuery(e).closest("div.row").find("input[placeholder=value]");

                jQuery($element).attr('type',val);
            }
        
</script>
@endsection

