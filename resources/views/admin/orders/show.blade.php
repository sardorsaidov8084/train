@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/lib/datatable/dataTables.bootstrap.min.css') }}">
    
@endsection
@section('content')
    <div id="right-panel" class="right-panel">
        @include('admin.includes.header')
        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                 <div class="row m-0">    
                    <div class="col-sm-8">
                        <div class="page-header float-left">
                            <div class="page-title">
                            <ol class="breadcrumb text-right">
                                @if(!auth()->user()->hasRole(App\Role::DOSTAVKA) && $order->user_id == auth()->id())
                                    <li><strong><a href="{{route('admin.orders.my_orders.index')}}">Мои Заказы </a></strong></li> 
                                    @else <li><strong><a href="{{route('admin.orders.index')}}">Заказы </a></strong></li> 
                                @endif

                                <li class="">Таблица</li>
                            </ol>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="page-header float-right">
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Таблица данных</strong>
                            </div>
                            <div class="table-stats order-table ov-h">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            <th>№</th>
                                            <th>Товарь</th>
                                            <th>Цена (1 кг/т)</th>
                                            <th>Количество</th>
                                            <th> Сумма</th>
                                            <th style="text-align:center">Статус</th>
                                            <th>Компания Имя</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php($inc=1)
                                       
                                            @foreach($orderProducts as $orderProduct)
                                            <tr>
                                                <td class="serial">{{$inc++}}</td>
                                                <td> <span class="">{{$orderProduct->products->name}}</span> </td>
                                             
                                                <td><span class="">{{ $orderProduct->product_price}}</span></td>
                                                <td>
                                                    <span class="">{{$orderProduct->quantity}} {{$orderProduct->weight}}</span>
                                                </td>
                                                <td>
                                                    <span class=""> {{$orderProduct->amount}}</span>
                                                </td>
                                                <td>
                                                    <span class="badge badge-success">{{ \App\OrderStatus::STATUES[$order->status->status] }}</span> 
                                                    @if($order->status->body)<span class=" badge badge-warning"> {{ $order->status->body }}</span>@endif
                                                </td>
                                                <td>{{$orderProduct->companies->name}}</td>
                                            </tr>
                                            @endforeach
                                        
                                    </tbody>
                                </table>
                               
                            </div> 
                            <!-- /.table-stats -->
                        </div>
                        
                       
                    </div>
                    <div class="col-md-4">
                        
                        <h5> <strong>ФИО: </strong>{{$order->user->name}}</h5>
                        <br>
                        <h5> <strong>Телефон: </strong>@if($order->user){{$order->user->phone}} @endif</h5>
                        <br>
                        <h5> <strong>Адрес: </strong>{{$order->address}}</h5>
                        <br>
                        @if($order->status->paid=='payment_complated')
                            <h5><strong>Уплатить: </strong>  {{ $order->amount }} 
                            </h5>
                            <br> 
                        @elseif($order->status->paid=='payment_paid')
                            <h5><strong>оплаченный : {{ $order->transactionAmount($order->id) }} UZB</strong>
                                
                            <h5>
                            <br>
                        @endif
                        <p style="font-size:20px;"> <span class="badge badge-warning">Общая оплата : {{$order->amount}} UZB</span> 
                        <!-- POKUPATEL va AGENT -->
                       
                        @if(auth()->user()->hasRole(App\Role::POKUPATEL) || auth()->user()->hasRole(App\Role::AGENT))
                            @include('admin.orders.orders_send.show')
                        @endif
                        <!-- POKUPATEL va AGENT tugatildi -->
                         <!-- DOSTAVKA va AGENT -->
                         @if($order->status->body==(App\Status::find(1))->name && auth()->user()->hasRole(App\Role::AGENT))
                            @if(($order->user_id == auth()->id()))
                                <div class="row">
                                    <div class="col-md-2" >
                                        @include('admin.modals.order_modal',['order' => $order])
                                    </div>
                                </div>
                           @endif
                        @endif
                        @if($order->status->body==(App\Status::find(1))->name)
                             @if(!($order->user_id == auth()->id()))
                             <div class="row">
                                <div class="col-md-2">
                                    <form  id="myForm" action="{{ route('admin.orders.my_orders.confirm') }}" method="post">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="order_id" value="{{ $order->id }}">
                                    
                                        <div>
                                        
                                            <div>
                                                <button type="submit" class="btn btn-success">Утверждать</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-3"></div>
                                <div class="col-md-2" >
                                    @include('admin.modals.order_modal',['order' => $order])
                                </div>
                             </div>
                            @endif
                        @endif
                            
                            
                        @if($order->status->body==(App\Status::find(3))->name && auth()->user()->hasRole(App\Role::DOSTAVKA))
                            @if(!($order->user_id == auth()->id()))
                             <div class="row">
                                <div class="col-md-2" >
                                    <form  id="myForm" action="{{ route('admin.orders.my_orders.wait') }}" method="post">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="order_id" value="{{ $order->id }}">
                                    
                                        <div>
                                        
                                            <div>
                                                <button type="submit" class="btn btn-danger">Ожидание</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                             </div>
                            @endif
                        @endif
                        @if(!auth()->user()->hasRole(App\Role::DOSTAVKA))
                            @if($order->status->paid!='payment_complated')
                            <div class="row">
                                    @if (Session::has('message'))
                                     <div class="alert alert-danger">{{ Session::get('message') }}</div>
                                     @endif
                                <div class="col-md-2 mt-2 mr-2 mb-2 ml-0" >
                                    @include('admin.modals.payment',['order' => $order])
                                </div>
                                <div class="col-md-2 m-2 ml-4" >
                                    @include('admin.modals.banks',['order' => $order])
                                </div>
                            </div>
                            @endif
                            <!-- <div class="row">
                                <div class="col-md-2" >
                                    <form  id="myForm" action="{{ route('admin.transactions.store') }}" method="post">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="order_id" value="{{ $order->id }}">
                                    
                                        <div>
                                            <div>
                                                <button type="submit" class="btn btn-success">Уплатить</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                             </div> -->
                        @endif
                       </p>
                       
                       
                    </div>

                </div>
                <!-- @if($order->status->body==(App\Status::find(2))->name)
                            
                 <h4><span class="badge badge-warning">Atmen qilganligi sababi: {{$order->status->delivery}}</span></h4>        
                @endif -->
            </div><!-- .animated -->

                @if(($order->status->body==(App\Status::find(3))->name)||($order->status->body==(App\Status::find(4))->name))
                    @if(auth()->user()->hasRole(\App\Role::DOSTAVKA))
                        @include('admin.shippings.create') 
                    @endif    
                @endif
            
                @if(($order->status->body==(App\Status::find(6))->name)|| ($order->status->body==(App\Status::find(5))->name))
                    @if(auth()->user()->hasRole(App\Role::AGENT)|| auth()->user()->hasRole(App\Role::DOSTAVKA))
                    
                        @include('admin.shippings.show', ['shipping'=>App\Shipping::where(['order_id' => $order->id])->first(),'order' =>$order]) 
                    @endif    
                @endif
            <!-- DOSTAVKA and AGENT close --> 
        </div>
       
        <div class="clearfix"></div>

      
    </div>
@endsection
@section('script')
    <script src="{{ asset('admin/js/lib/data-table/datatables.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/jszip.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/vfs_fonts.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.print.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('admin/js/init/datatables-init.js') }}"></script>
    <script src="{{ asset('/admin/js/datepicker.min.js')}}"></script>
    <script src="{{ asset('admin/js/i18n/datepicker.en.js') }}"></script>
    <script>
    
    // Create start date
    var start = new Date(),
        prevDay,
        startHours = 9;

    // 09:00 AM
    start.setHours(9);
    start.setMinutes(0);

    // If today is Saturday or Sunday set 10:00 AM
    if ([6, 0].indexOf(start.getDay()) != -1) {
        start.setHours(10);
        startHours = 10
    }

    $('#timepicker-actions-exmpl').datepicker({
        timepicker: true,
        language: 'en',
        startDate: start,
        minHours: startHours,
        maxHours: 18,
        onSelect: function (fd, d, picker) {
            // Do nothing if selection was cleared
            if (!d) return;

            var day = d.getDay();

            // Trigger only if date is changed
            if (prevDay != undefined && prevDay == day) return;
            prevDay = day;

            // If chosen day is Saturday or Sunday when set
            // hour value for weekends, else restore defaults
            if (day == 6 || day == 0) {
                picker.update({
                    minHours: 10,
                    maxHours: 16
                })
            } else {
                picker.update({
                    minHours: 9,
                    maxHours: 18
                })
            }
        }
    })
    $('#timepicker-actions-exmpl_new').datepicker({
        timepicker: true,
        language: 'en',
        startDate: start,
        minHours: startHours,
        maxHours: 18,
        onSelect: function (fd, d, picker) {
            // Do nothing if selection was cleared
            if (!d) return;

            var day = d.getDay();

            // Trigger only if date is changed
            if (prevDay != undefined && prevDay == day) return;
            prevDay = day;

            // If chosen day is Saturday or Sunday when set
            // hour value for weekends, else restore defaults
            if (day == 6 || day == 0) {
                picker.update({
                    minHours: 10,
                    maxHours: 16
                })
            } else {
                picker.update({
                    minHours: 9,
                    maxHours: 18
                })
            }
        }
    })
</script>
    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
          

      } );
    $(function(){
        $('#from_country_id').change(function(){
                var val= $('#from_country_id').val();
                var url = '/admin/orders/order_from_station/' + val;
                jQuery.ajax({
                url:url,
                type: "get",
                async: false,
                success: function(data){
                   stations = data;
                    text = "";        
                    text = '<option>Выберите станцию </option>';
                    for(var i =0; i < stations.length; i++){
                        text = text + '<option value=\"' + stations[i].id + '\">' + stations[i].name + '</option>';
                        
                    }   
                    $('#from_station_id').html(text);
                    

                }
            });
        });
        $('#to_country_id').change(function(){
                var val= $('#to_country_id').val();
                var url = '/admin/orders/order_from_station/' + val;
                jQuery.ajax({
                url:url,
                type: "get",
                async: false,
                success: function(data){
                   stations = data;
                    text = "";        
                    text = '<option>Выберите станцию </option>';
                    for(var i =0; i < stations.length; i++){
                        text = text + '<option value=\"' + stations[i].id + '\">' + stations[i].name + '</option>';
                        
                    }   
                    $('#to_station_id').html(text);
                    

                }
            });
        });
        $('#direction_id').change(function(){
                var val= $('#direction_id').val();
                var url = '/admin/orders/order_train/' + val;
               
                jQuery.ajax({
                url:url,
                type: "get",
                async: false,
                success: function(data){
                    
                   trains = data;
                    text = "";        
                    text = '<option>Выберите поезд </option>';
                    for(var i =0; i < trains.length; i++){
                        text = text + '<option value=\"' + trains[i].id + '\">' + trains[i].name + '</option>';
                        
                    }   
                    $('#train_id').html(text);
                    

                }
            });
        });
        $('#train_id').change(function(){
                var val= $('#train_id').val();
                var url = '/admin/orders/order_wagon/' + val;
               
                jQuery.ajax({
                url:url,
                type: "get",
                async: false,
                success: function(data){
                    
                   wagon = data;
                    text = "";        
                    text = '<option>Выберите вагон </option>';
                    for(var i =0; i < wagon.length; i++){
                        text = text + '<option value=\"' + wagon[i].id + '\">' + wagon[i].name + '</option>';
                        
                    }   
                    $('#wagon_id').html(text);
                    

                }
            });
        });
        $('#wagon_id').change(function(){
                var val= $('#wagon_id').val();
                var url = '/admin/orders/order_container/' + val;
               
                jQuery.ajax({
                url:url,
                type: "get",
                async: false,
                success: function(data){
                    
                   container = data;
                    text = "";        
                    text = '<option>Выберите контейнер</option>';
                    for(var i =0; i < container.length; i++){
                        text = text + '<option value=\"' + container[i].id + '\">' + container[i].name + '</option>';
                        
                    }   
                    $('#container_id').html(text);
                    

                }
            });
        });
    });
    
</script>
    
@endsection
