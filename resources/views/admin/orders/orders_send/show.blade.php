@if($order->status->body==(App\Status::find(1))->name && auth()->user()->hasRole(App\Role::POKUPATEL))
    @if(($order->user_id == auth()->id()))
        <div class="row">
            <div class="col-md-2" >
            @include('admin.modals.order_modal',['order' => $order])
            </div>
        </div>
    @endif
@endif
@if($order->status->body==(App\Status::find(3))->name && auth()->user()->hasRole(App\Role::AGENT))
    @if(!($order->user_id == auth()->id()))
        <div class="row">
            <div class="col-md-2" >
                <form  id="myForm" action="{{ route('admin.orders.my_orders.ready') }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="order_id" value="{{ $order->id }}">
                
                    <div>
                    
                        <div>
                            <button type="submit" class="btn btn-success">Отправить</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-2" >
                <form  id="myForm" action="{{ route('admin.orders.my_orders.wait') }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="order_id" value="{{ $order->id }}">
                
                    <div>
                    
                        <div>
                            <button type="submit" class="btn btn-danger">Ожидание</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endif
@endif
@if($order->status->body==(App\Status::find(4))->name && auth()->user()->hasRole(App\Role::AGENT))
    @if(!($order->user_id == auth()->id()))
        <div class="row">
            <div class="col-md-2" >
                <form  id="myForm" action="{{ route('admin.orders.my_orders.ready') }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="order_id" value="{{ $order->id }}">
                
                    <div>
                    
                        <div>
                            <button type="submit" class="btn btn-success">Отправить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div> 
    @endif   
@endif
@if($order->status->body==(App\Status::find(5))->name && auth()->user()->hasRole(App\Role::POKUPATEL))
    @if($order->user_id == auth()->id())
        <div class="row">
            <div class="col-md-2" >
                <form  id="myForm" action="{{ route('admin.orders.complete') }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="order_id" value="{{ $order->id }}">
                
                    <div>
                    
                        <div>
                            <button type="submit" class="btn btn-primary">Принятие</button>
                        </div>
                    </div>
                </form>
            </div>
           
        </div>
    @endif
@endif       