@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/lib/chosen/chosen.min.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <style>
        .chosen-container{
            margin-left:15px;
            width:83% !important;
        }
    </style>
   
@endsection
@section('content')
<div id="right-panel" class="right-panel">
        
  @include('admin.includes.header')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-5">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><strong><a href="{{route('admin.orders.index')}}">Заказы</a></strong></li>
                                <li class="">Добавить </li>
                            </ol>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Форма элементов</strong> 
                        </div>
                        <div class="card-body">
                            <div class="card-body card-block">
                                <form action="{{ route('admin.orders.update',['order' => $order->id])}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    @csrf
                                    @method('PUT')
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="user_id" class=" form-control-label">Имя</label>
                                                </div>
                                                <select data-placeholder="" class="standardSelect" name ="user_id"tabindex="1">
                                                        <option value="">...</option>
                                                        @foreach(App\User::latest()->get() as $user)
                                                            <option value="{{ $user->id}}" @if($order->user->id==$user->id) selected @endif > {{ $user->name }}</option>
                                                        @endforeach
                                                 </select>
                                                    @if($errors->has('user_id'))
                                                        <span style="font-size: 10px; color: red; float: left;margin-left:15px;">{{ $errors->first('user_id') }}</span>
                                                    @endif
                                            </div>
                                        </div>    
                                         
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="address" class=" form-control-label">Адрес</label>
                                                </div>
                                                <div class="col-12 col-sm-9">
                                                    <textarea name="address" id="address" cols="" rows="5" class="form-control">{{ $order->address }}</textarea>
                                                    @if($errors->has('address'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('address') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="category_attribut_row">
                                        <div class="row">
                                             <div class="col-12 col-sm-9">
                                                <label for="" class=" form-control-label">Продукты</label>
                                            </div>
                                        </div>     
                                     </div>

                                     <!--  @foreach($orderProducts as $orderProduct)
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <select class="form-control">
                                                    @foreach($products as $product)
                                                    <option @if($orderProduct->products->id==$product->id) selected  @endif>{{ $product->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input class="form-control" type="number" name="product" placeholder="Цена (1 кг)" value="{{ $orderProduct->product_price}}">
                                            </div>
                                        </div>   
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input type="number" class="form-control" name="quantity" placeholder="кг" value="{{$orderProduct->quantity}}">  
                                             </div>
                                        </div>
                                        <div class="col-md-1">
                                            <span class="delete btn btn-danger"><i class="fa fa-trash"></i></span>
                                        </div>
                                    </div>
                                    @endforeach -->
                                    
                                    <div id="category_attribut_rows">
                                    </div>
                                     <div class="row">
                                        <div class=" form-group">
                                                        
                                            <div class="col-12 col-sm-9">
                                                <span id="addAttribute" class="btn btn-info" >Добавить Продукты</span>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row form-group">
                                        <div class="col-12 col-sm-9">
                                            <button type="submit" class="btn btn-primary">Добавить</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>                     
                </div>
            </div>
            
        </div><!-- .animated -->
    </div><!-- .content -->

    <div class="clearfix"></div>

</div><!-- /#right-panel -->

@endsection
@section('script')
<script src="{{ asset('admin/js/lib/chosen/chosen.jquery.min.js')}}"></script>
<script>

    jQuery(document).ready(function() {
        jQuery(".standardSelect").chosen({
            disable_search_threshold: 10,
            no_results_text: "Oops, nothing found!",
            width: "100%"
        });
        
    });
    let stationsGet = function(order_id){
        var data;
        jQuery.ajax({
                    url: "{{ route('admin.productSelect')}}",
                    type:'get',
                    async: false,
         success: function(results){
            text = "";        
             text = text +'<option value >' + '</option>';
             for(var i =0; i < results.length; i++){
                if(order_id==results[i].id)
                {
                    text = text + '<option selected data-p_id=\"'+results[i].id+'\" value=\"' + results[i].id + '\">' + results[i].name + '</option>';
                }else
                {
                   text = text + '<option  data-p_id=\"'+results[i].id+'\" value=\"' + results[i].id + '\">' + results[i].name + '</option>'; 
                }                
                 
             }   
             data = text;
  
        }});
        return data;
    }
    let counter = 0;

    <?php foreach ($orderProducts as $orderProduct): ?>
        
        counter++;                
                    let a_<?php echo $orderProduct->id ?> = "<div class=\"row\" id=\""+counter+"\" style=\"margin-bottom:20px !important;\">\n" +
                   
                     "<div class=\"col-md-5\">\n" +
                        "<div class=\" form-group\">"+
                        "<select data-counter=\""+counter+"\" data-placeholder=\"\" name=\"product["+counter+"][id]\" required class=\"form-control\" id=\"product_id\" tabindex=\"1\" onchange=\"changeInputTypeBySelect(this)\">\n" +
                        stationsGet(<?php echo $orderProduct->products->id ?>)                        
                        + "</select>\n" +
                        "</div>"+
                    "</div>\n" +
                    "<input type=\"hidden\" value=\"<?php echo $orderProduct->id ?>\" >\n" +
                    "<div class=\"col-md-3\">\n" +
                    "<input class=\"form-control\" type=\"number\" name=\"product["+counter+"][price]\" id=\"price-"+counter+"\" value=\"<?php echo $orderProduct->product_price ?>\"  required placeholder=\"Цена (1 кг)\">\n" +
                    "</div>\n" +
                    "<div class=\"col-md-2\">\n" +
                    "<input class=\"form-control\" type=\"number\" name=\"product["+counter+"][quantity]\" id=\"quantity\" value=\"<?php echo $orderProduct->quantity ?>\" required placeholder=\"кг\">\n" +
                    "</div>\n" +
                    "<div class=\"col-md-1\">\n"+
                        "<span onclick='removeThis(this)' class=\"delete btn btn-danger \" data-id="+counter+"><i class=\"fa fa-trash\"></i></span>\n" +
                     "</div>"+
                    "</div>\n";
                
                jQuery('#category_attribut_rows').append(a_<?php echo $orderProduct->id ?>);
                
    <?php endforeach ?>
    
    jQuery("#addAttribute").click(function () {
                counter++;                
                    let row = "<div class=\"row\" id=\""+counter+"\" style=\"margin-bottom:20px !important;\">\n" +
                   
                     "<div class=\"col-md-5\">\n" +
                        "<div class=\" form-group\">"+
                        "<select data-counter=\""+counter+"\" data-placeholder=\"\" name=\"product["+counter+"][id]\" required class=\"form-control\" id=\"product_id\" tabindex=\"1\" onchange=\"changeInputTypeBySelect(this)\">\n" +
                        stationsGet()                        
                        + "</select>\n" +
                        "</div>"+
                    "</div>\n" +
                    "<div class=\"col-md-3\">\n" +
                    "<input class=\"form-control\" type=\"number\" name=\"product["+counter+"][price]\" id=\"price-"+counter+"\"  required placeholder=\"Цена (1 кг)\">\n" +
                    "</div>\n" +
                    "<div class=\"col-md-2\">\n" +
                    "<input class=\"form-control\" type=\"number\" name=\"product["+counter+"][quantity]\" id=\"quantity\" required placeholder=\"кг\">\n" +
                    "</div>\n" +
                    "<div class=\"col-md-1\">\n"+
                        "<span onclick='removeThis(this)' class=\"delete btn btn-danger \" data-id="+counter+"><i class=\"fa fa-trash\"></i></span>\n" +
                     "</div>"+
                    "</div>\n";
                
                jQuery('#category_attribut_rows').append(row);
            });

            function removeThis(e) {
                jQuery("#" + jQuery(e).data('id')).remove();
               
            }
            function changeInputTypeBySelect(e){
             let price_counter = jQuery(e).data('counter');   
            let val =jQuery(e).val();
             var url = '/admin/orders/orderPrice/' + val;
            jQuery.ajax({
            url: url,
            type: 'get',
            success: function (data) {
                jQuery("#price-"+price_counter).val(data.price);
            }
        });
                let $element =jQuery(e).closest("div.row").find("input[placeholder=value]");

                jQuery($element).attr('type',val);
            }
</script>
@endsection
