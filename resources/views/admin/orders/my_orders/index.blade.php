@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/lib/datatable/dataTables.bootstrap.min.css') }}">
    
@endsection
@section('content')
    <div id="right-panel" class="right-panel">
        @include('admin.includes.header')
        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                 <div class="row m-0">    
                    <div class="col-sm-8">
                        <div class="page-header float-left">
                            <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><strong><a href="#">Мои заказы </a></strong></li>
                                <li class="">Таблица</li>
                            </ol>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="page-header float-right">
                            <div class="page-title">
                                @permission('create.orders')
                                    <a href="{{ route('admin.orders.create') }}" class="btn btn-primary btn-square" style="float: left;margin-top:7px;"><i class="ti-plus"></i> Добавить  </a>
                                @endpermission
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Таблица данных</strong>
                            </div>
                            <div class="card-body">
                                  <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>№</th>
                                            <th>Имя</th>
                                            <th>Общая стоимость</th>
                                            <th>Статус</th>
                                            <th>Время</th>
                                            <th>Настройка</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php($inc=1)
                                        @foreach($orders as $order)
                                        <tr>
                                            <td>{{$inc++}}</td>
                                            
                                            @if($order->user)
                                            <td>{{$order->user->name}}</td>
                                                @else <td> без имени</td>
                                            @endif 
                                            
                                            <td> {{$order->amount}} </td>
                                            <td>
                                                <span class="badge badge-success">{{ \App\OrderStatus::STATUES[$order->status->status] }}</span>  
                                                <span class=" badge badge-warning"> {{ $order->status->body }}</span> 
                                            </td>
                                            <td> {{$order->created_at}} </td>
                                            <td class="text-center">
                                                <a class="btn btn-primary btn-xs" href="{{route('admin.orders.show',['order'=>$order->id])}}"><i class="fa fa-eye"></i></a>
                                            @if($order->status->body =='Zakaz berildi.' || $order->status->body =='Zakaz ko`rildi')   
                                                @permission('edit.orders')
                                                    <a class="btn btn-primary btn-xs" href="{{route('admin.orders.edit',['order'=>$order->id])}}"><i class="fa fa-pencil"></i></a>
                                                @endpermission
                                                @permission('delete.orders')
                                                    <form action="{{route('admin.orders.destroy',['order'=>$order->id])}}" method="post" style="display:inline-block">
                                                        @csrf
                                                        @method('delete')
                                                        <button class="btn btn-danger btn-xs"  onclick="return confirm('Вы действительно хотите удалить его?')"  type="submit"><i class="fa fa-trash"></i></button>
                                                    </form>	
                                                @endpermission 

                                            @endif    
											</td>
                                            
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>                             
                            </div>
                        </div>
                    </div>


                </div>
            </div><!-- .animated -->
        </div>
       
        <div class="clearfix"></div>

        <!-- @include('admin.includes.footer') -->
    </div>
@endsection
@section('script')
    <script src="{{ asset('admin/js/lib/data-table/datatables.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/jszip.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/vfs_fonts.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.print.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('admin/js/init/datatables-init.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
      } );
    </script>
    
@endsection
