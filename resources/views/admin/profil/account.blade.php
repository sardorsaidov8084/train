@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/lib/datatable/dataTables.bootstrap.min.css') }}">
    
@endsection
@section('content')
    <div id="right-panel" class="right-panel">
        @include('admin.includes.header')
         <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                 <div class="row m-0">    
                    <div class="col-sm-8">
                        <div class="page-header float-left">
                            <div class="page-title">
                            <ol class="breadcrumb text-right">
                                    <li><strong><a href="{{route('admin.profil.account')}}">User </a></strong></li> 
                                    

                                <li class="">Profil</li>
                            </ol>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="page-header float-right">
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-8">   
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title"> Orders данных</strong>
                            </div>
                            <div class="table-stats order-table ov-h">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            <th>№</th>
                                            <th>address</th>
                                            <th>amount</th>
                                            <th>User</th>
                                            <th>Created_by</th>
                                            <th>Status</th>
                                            <th> date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($orders as $order)
                                            <tr>
                                                <td>#{{ $order->id }}</td>
                                                <td>{{ $order->address }}</td>
                                                <td>{{ $order->amount }}</td>
                                                <td>{{ $order->user->name }}</td>
                                                <td>{{ $order->createdUser->name }}</td>
                                                <td>{{ $order->status->body }}, {{ $order->status->paid}}</td>
                                                <td>{{ $order->created_at }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                               
                            </div> 
                        </div>


                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Qarzlar</strong>
                            </div>
                            <div class="table-stats order-table ov-h">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            <th>№</th>
                                            <th>Amount</th>
                                            <th>currency</th>
                                            <th>Kimga berish kerak</th>
                                            <th>Kimning qarzi</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($bankUsers as $bankUser)
                                            @if($bankUser->receiver->id==auth()->id()||$bankUser->creater->id==auth()->id())
                                            <tr>
                                                <td>#{{ $bankUser->id }}</td>
                                                <td>{{ $bankUser->amountall }}</td>
                                                <td>UZB</td>
                                                <td>{{ $bankUser->receiver->name }}</td>
                                                <td>{{ $bankUser->creater->name }} </td>
                                                <td>
                                                    pay
                                                </td>
                                            </tr>
                                            @endif
                                       @endforeach
                                    </tbody>
                                </table>
                               
                            </div> 
                        </div>


                        @if(!auth()->user()->hasRole(App\Role::DOSTAVKA))
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Siznign Qarzlaringiz</strong>
                            </div>
                            <div class="table-stats order-table ov-h">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            <th>№</th>
                                            <th>Amount</th>
                                            <th>currency</th>
                                            <th>receiver</th>
                                            <th>Accepted</th>
                                            <th>date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($banks as $bank)
                                            @if($bank->creater->id==auth()->id())
                                            <tr>
                                                <td>#{{ $bank->id }}</td>
                                                <td>{{ $bank->amount }}</td>
                                                <td>UZB</td>
                                                <td>{{ $bank->receiver->name }}</td>
                                                <td>
                                                    {{ $bank->accepted }}
                                                </td>
                                                <td>{{ $bank->created_at }}</td>
                                            </tr>
                                            @endif

                                       @endforeach
                                    </tbody>
                                </table>
                               
                            </div> 
                        </div>

                        @endif

                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Qarzdorlar</strong>
                            </div>
                            <div class="table-stats order-table ov-h">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            <th>№</th>
                                            <th>Amount</th>
                                            <th> currency</th>
                                            <th>Creater</th>
                                            <th>Accepted</th>
                                            <th>date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($banks as $bank)
                                            @if($bank->receiver->id==auth()->id())
                                            <tr>
                                                <td>#{{ $bank->id }}</td>
                                                <td>{{ $bank->amount }}</td>
                                                <td>UZB</td>
                                                <td>{{ $bank->creater->name}}</td>
                                                <td>
                                                    {{ $bank->accepted }}
                                                    @if(!$bank->accepted && $bank->receiver->id==auth()->id())
                                                        <form  action="{{ route('admin.banks.user_check') }}" method="post" >
                                                            @csrf
                                                            <input type="hidden" name="bank_id" id="bank_id" value="{{ $bank->id }}">
                                                            <input type="checkbox" id="accepted" required name="accepted">
                                                            <input type="submit" value="Yangilash">
                                                        </form>
                                                    @endif
                                                </td>
                                                <td>{{ $bank->created_at }}</td>
                                            </tr>
                                            @endif
                                       @endforeach
                                    </tbody>
                                </table>
                               
                            </div> 
                        </div>

                        
                        @if(!auth()->user()->hasRole(App\Role::POKUPATEL))
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Transactions данных</strong>
                            </div>
                            <div class="table-stats order-table ov-h">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            <th>№</th>
                                            <th>Amount</th>
                                            <th> currency</th>
                                            <th>Order ID</th>
                                            <th>income</th>
                                            <th>receiver</th>
                                            <th>Creater</th>

                                            <th>accepted</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($user_transactions as $u_transaction)
                                        @foreach($transactions as $transaction)
                                            @if($transaction->id==$u_transaction->transaction_id&&$transaction->income==true)
                                            <tr>
                                                <td>#{{ $transaction->id }}</td>
                                                <td>{{ $transaction->amount }}</td>
                                                <td>{{ $transaction->currency }}</td>
                                                <td>#{{ $transaction->orderID($transaction->id) }}</td>
                                                <td>{{ $transaction->income }}</td>
                                                <td>
                                                    {{ $transaction->users->receiver->name }}
                                                </td>
                                                <td>
                                                    {{ $transaction->users->creater->name}}
                                                </td>
                                                <td>{{ $transaction->accepted }}
                                                    @if(!$transaction->accepted)
                                                        <form  action="{{ route('admin.transactions.user_check') }}" method="post" >
                                                            @csrf
                                                            <input type="hidden" name="transaction_id" id="transaction_id" value="{{ $transaction->id }}">
                                                            <input type="checkbox" id="accepted" required name="accepted">
                                                            <input type="submit" value="Yangilash">
                                                        </form>
                                                    @endif
                                                </td>
                                                <td>{{ $transaction->created_at }}</td>
                                            </tr>
                                            @endif
                                        @endforeach
                                       @endforeach
                                    </tbody>
                                </table>
                               
                            </div> 
                            <!-- /.table-stats -->
                        </div>
                        @endif

                        @if(!auth()->user()->hasRole(App\Role::DOSTAVKA))
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title"> Created Transactions данных</strong>
                            </div>
                            <div class="table-stats order-table ov-h">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            <th>№</th>
                                            <th>Amount</th>
                                            <th> currency</th>
                                            <th>Order ID</th>
                                            <th>income</th>
                                            <th>receiver</th>
                                            <th>Creater</th>

                                            <th>accepted</th>
                                            <th>date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($created_transactions as $creatd_transaction)
                                        @foreach($transactions as $transaction)
                                            @if($transaction->id==$creatd_transaction->transaction_id && $transaction->income==false)
                                            <tr>
                                                <td>#{{ $transaction->id }}</td>
                                                <td>{{ $transaction->amount }}</td>
                                                <td>{{ $transaction->currency }}</td>
                                                <td>#{{ $transaction->orderID($transaction->id) }}</td>
                                                <td>{{ $transaction->income }}</td>
                                                <td>{{ $transaction->users->receiver->name }}</td>
                                                <td>{{ $transaction->users->creater->name}}</td>
                                                <td>{{ $transaction->accepted }}</td>
                                                <td>{{ $transaction->created_at }}</td>
                                            </tr>
                                            @endif
                                        @endforeach
                                       @endforeach
                                    </tbody>
                                </table>
                               
                            </div> 
                            <!-- /.table-stats -->
                        </div>
                        @endif


                       
                </div>
                <div class="col-md-4">
                        <h5> <strong>ФИО: </strong> {{ auth()->user()->name }}</h5>
                        <br>
                        <h5> <strong>Телефон:  </strong>{{ auth()->user()->phone }}</h5>
                        <br>
                       
                        <p style="font-size:20px;"> <span class="badge badge-warning">Общая оплата : {{ auth()->user()->cash }}  сум</span> 
                        
                        <br>
                        <h5> <strong>Siz qabul qilgan summa :  </strong>{{ auth()->user()->income(true) }} UZB</h5>
                        <br>

                        <h5> <strong>Siz tulagan summa :  </strong>{{ auth()->user()->income(false) }} UZB</h5>
                        <br>
                        <h5> <strong>Sizning qarzlaringiz summasi :   </strong> {{ auth()->user()->debt() }} UZB</h5>
                        <br>
                        <h5> <strong>Sizdan qarzlar summasi :  </strong>{{auth()->user()->debtor() }}  UZB</h5>
                        <br>
                        @if(!auth()->user()->hasRole('dostavka'))
                            @include('admin.modals.bank_create',['users'=>$users])
                        @endif
                       </p>
                       
                       
                    </div>
                </div>
            </div>
                
            </div>

            <!-- DOSTAVKA and AGENT close --> 
        </div>
       
        <div class="clearfix"></div>

      
    </div>
@endsection
@section('script')
    <script src="{{ asset('admin/js/lib/data-table/datatables.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/jszip.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/vfs_fonts.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.print.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('admin/js/init/datatables-init.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
         
      } );


    $(function(){
        $('#accepted').change(function(){
                var val= $('#transaction_id').val();
                var url = '/admin/profil/account/checkbox/' + val;
                jQuery.ajax({
                url:url,
                type: "post",
                async: false,
                success: function(data){
                    return data;
                }
            });
        });
    });
    

   
</script>
    
@endsection
