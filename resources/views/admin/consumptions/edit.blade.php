@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/lib/chosen/chosen.min.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <style>
    .chosen-container{
        margin-left:15px;
    }
    </style>
   
@endsection
@section('content')
<div id="right-panel" class="right-panel">

  @include('admin.includes.header')
  <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-5">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><strong><a href="{{route('admin.consumptions.index')}}">Расход</a></strong></li>
                                <li class="">Обновить </li>
                            </ol>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Форма элементов</strong> 
                        </div>
                        <div class="card-body">
                            <div class="card-body card-block">
                                <form action="{{ route('admin.consumptions.update',['id' =>$consumption->id])}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    @csrf
                                    @method('PUT')
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="name" class=" form-control-label">Заглавие</label>
                                                </div>
                                                <div class="col-12 col-sm-9">
                                                    <input type="text" id="title" name="title" placeholder="" class="form-control" value="{{$consumption->title}}">
                                                    @if($errors->has('title'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('title') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="name" class=" form-control-label">Сумма</label>
                                                </div>
                                                <div class="col-12 col-sm-9">
                                                    <input type="number" id="amount" name="amount"  class="form-control" value="{{$consumption->amount}}">
                                                    @if($errors->has('amount'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('amount') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row form-group">
                                               
                                                <div class="col-12 col-sm-9">
                                                    <textarea type="text" id="description" name="description" placeholder="Описание" class="form-control" rows="4">{{$consumption->description}}</textarea>
                                                    @if($errors->has('description'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('description') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            
                                        </div>
                                      </div>
                                   
                                    <br>
                                    <div class="row form-group">
                                        <div class="col-12 col-sm-9">
                                            <button type="submit" class="btn btn-primary">Обновить </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>                     
                </div>
            </div>
            
        </div><!-- .animated -->
    </div><!-- .content -->

    <div class="clearfix"></div>

</div><!-- /#right-panel -->

@endsection
@section('script')
<script src="{{ asset('admin/js/lib/chosen/chosen.jquery.min.js')}}"></script>

<script>
    jQuery(document).ready(function() {
        jQuery(".standardSelect").chosen({
            disable_search_threshold: 10,
            no_results_text: "Oops,Ничего не найдено!",
            width: "100%"
        });
    });
</script>
@endsection
