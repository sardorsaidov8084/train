
@if($shipping)   
     
   <div class="animated fadeIn">
       <div class="row">
           <div class="col-md-12">
               <div class="card">
                   <!-- <div class="card-header">
                       <strong class="card-title">Таблица данных</strong>
                   </div> -->
                   <div class="">
                        <div class="row">
                            <div class="col-md-12">
                                <table  class="table table-striped table-bordered">
                                    <tr>
                                        <th>Состояние выхода</th>  
                                        <th>Выходная станция</th>
                                        <th>Состояние входа</th>
                                        <th>Входа станция</th>
                                        <th>Время выхода </th>
                                        <th>Время идти</th>
                                    </tr>
                                    <tr>
                                        <td> {{ (App\Country::find($shipping->from_country))->name }} </td>
                                        <td> {{ (App\Station::find($shipping->from_station))->name }}</td>
                                        <td> {{ (App\Country::find($shipping->to_country))->name }} </td>
                                        <td> {{ (App\Station::find($shipping->to_station))->name }} </td> 
                                        <td> {{ $shipping->from_date }} </td>   
                                        <td> {{ $shipping->to_date }} </td>
                                    </tr>
                                </table> 
                            </div> 
                           
                        </div>    
                        <div class="row">
                            <div class="col-md-7">
                                <table  class="table table-striped table-bordered">
                                    <tr>
                                        <th>Направления</th>  
                                        <th>Поезд </th>
                                        <th>Вагон</th>
                                        <th>Контейнер</th>
                                        
                                    </tr>
                                    <tr>
                                        <td> {{ (App\Direction::find($shipping->direction_id))->name }} </td>
                                        <td> {{ (App\Station::find($shipping->train_id))->name }}</td>
                                        <td> {{ App\Wagon::find($shipping->wagon_id)->name}} </td>
                                        <td> {{  App\Container::find($shipping->container_id)->name }} </td> 
                                        
                                    </tr>
                                </table> 
                            </div> 
                           <div class="col-md-5">
                           <?php  $data =strtotime($shipping->to_date)-time();
                            $time = $data;
                            $days = floor($time / (24*60*60));
                            $hours = floor(($time - ($days*24*60*60)) / (60*60));
                            $minutes = floor(($time - ($days*24*60*60)-($hours*60*60)) / 60);
                            $station_from = time()-strtotime($shipping->from_date);
                            $station_day= floor($station_from / (24*60*60));
                           ?>
                                @if($data >=0)
                                    <h4>Manzilga borish uchun: <?php echo $days.' kun '.$hours.' soat '.$minutes.' minut'; ?>  qoldi </h4>
                                @endif   
                                   
                           </div>   
                           
                        </div>                    
                   </div>
               </div>
               @if($order->status->body==(App\Status::find(5))->name && auth()->user()->hasRole(App\Role::AGENT))
                   @if($order->user_id == auth()->id())
                       <div class="row">
                            <div class="col-md-2" >
                                <form  id="myForm" action="{{ route('admin.orders.complete') }}" method="post">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="order_id" value="{{ $order->id }}">
                                
                                    <div>
                                    
                                        <div>
                                            <button type="submit" class="btn btn-success">Принятие</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                           
                       </div>
                   @endif
               @endif        
           </div>


       </div>
   </div><!-- .animated -->

@endif      
