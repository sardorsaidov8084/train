<div class="animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>Форма элементов</strong> 
                </div>
                <div class="card-body">
                    <div class="card-body card-block">
                        <form action="{{ route('admin.shippings.store')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                            @csrf
                            <input type="hidden" name="order_id" value="{{ $order->id }}">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row form-group">
                                        <div class="col-12 col-sm-9">
                                            <label for="address" class=" form-control-label">Состояние выхода</label>
                                        </div>
                                        <div class="col-12 col-sm-9">
                                        <select  name="from_country" id ="from_country_id" class="form-control" data-search="true">
                                                <option value=""></option>
                                                @foreach(\App\Country::get() as $country)
                                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                                @endforeach
                                        </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row form-group">
                                        <div class="col-12 col-sm-9">
                                            <label for="address" class=" form-control-label">Состояние входа</label>
                                        </div>
                                        <div class="col-12 col-sm-9">

                                            <select  name="to_country" id="to_country_id" class="form-control" >

                                                <option value=""></option>
                                                @foreach(\App\Country::latest()->get() as $country)
                                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="row form-group">
                                        <div class="col-12 col-sm-9">
                                            <label for="address" class=" form-control-label">Выходная станция</label>
                                        </div>
                                        <div class="col-12 col-sm-9">
                                        <select name="from_station" id="from_station_id" class="form-control">
                                        
                                        </select>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="col-md-6">
                                    <div class="row form-group">
                                        <div class="col-12 col-sm-9">
                                            <label for="address" class=" form-control-label"> Входа станция</label>
                                        </div>
                                        <div class="col-12 col-sm-9">
                                        <select name="to_station" id="to_station_id" class="form-control">
                                        
                                        </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row form-group">
                                        <div class="col-12 col-sm-9">
                                            <label for="address" class=" form-control-label">Направления</label>
                                        </div>
                                        <div class="col-12 col-sm-9">
                                        <select  name="direction_id" id = "direction_id" class="form-control">
                                                <option value=""></option>
                                                @foreach(\App\Direction::latest()->get() as $direction)
                                                    <option value="{{$direction->id}}">{{$direction->name}}</option>
                                                @endforeach
                                        </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row form-group">
                                        <div class="col-12 col-sm-9">
                                            <label for="address" class=" form-control-label">Выберите поезд</label>
                                        </div>
                                        <div class="col-12 col-sm-9">
                                        <select name="train_id" id="train_id" class="form-control">
                                        
                                        </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row form-group">
                                        <div class="col-12 col-sm-9">
                                            <label for="address" class=" form-control-label">Выберите вагон</label>
                                        </div>
                                        <div class="col-12 col-sm-9">
                                        <select  name="wagon_id" id="wagon_id" class="form-control">
                                        </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row form-group">
                                        <div class="col-12 col-sm-9">
                                            <label for="address" class=" form-control-label"> Контейнер</label>
                                        </div>
                                        <div class="col-12 col-sm-9">
                                        <select name="container_id" id="container_id" class="form-control">
                                        
                                        </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row form-group">
                                        <div class="col-12 col-sm-9">
                                            <label for="address" class=" form-control-label">Время выхода </label>
                                        </div>
                                        <div class="col-12 col-sm-9">
                                        <input type='text' id='timepicker-actions-exmpl' class="form-control" name="from_date">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row form-group">
                                        <div class="col-12 col-sm-9">
                                            <label for="address" class=" form-control-label">Время входа</label>
                                        </div>
                                        <div class="col-12 col-sm-9">
                                          
                                        <input type='text' id='timepicker-actions-exmpl_new' class="form-control" name="to_date">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row form-group">
                                <div class="col-12 col-sm-9">
                                    <input type="hidden" name="order_id" value="{{$order->id}}">
                                    <button type="submit" class="btn btn-primary">Отправить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>                     
        </div>
    </div>

</div><!-- .animated -->

