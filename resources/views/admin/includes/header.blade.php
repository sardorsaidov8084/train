<header id="header" class="header">
    
    <div class="top-right">
        <div class="header-menu">
            <ul class="nav navbar-nav own-flex">
                <li class="active">
                    <a href="{{URL::to('admin/index')}}"><i class="menu-icon fa fa-laptop"></i>Главная страница </a>
                </li>
                @if(auth()->user()->hasRole(App\Role::AGENT))
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Пользователи</a>
                        <ul class="sub-menu children dropdown-menu">
                        
                            <li><i class=""></i><a href="{{ route('admin.users.index')}}">Клиенты</a></li>
                            
                        </ul>
                    </li>
                @endif
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-qrcode"></i>Каталог</a>
                    <ul class="sub-menu children dropdown-menu">
                       
                            <li><i class=""></i><a href="{{ route('admin.categories.index')}}">Категории</a></li>
                            <li><i class=""></i><a href="{{ route('admin.companies.index')}}">Компания</a></li>
                            <li><i class=""></i><a href="{{ route('admin.products.index')}}">Продукты</a></li>
                            <li><i class=""></i><a href="{{ route('admin.stores.index')}}">Склад</a></li>
                       
                    </ul>
                </li>
                
             <!-- /.menu-title -->
                @if(auth()->user()->hasRole(App\Role::AGENT)||auth()->user()->hasRole(App\Role::DOSTAVKA)) 
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-truck"></i>Лoгистика</a>
                        <ul class="sub-menu children dropdown-menu">
                            
                                <li><i class=""></i><a href="{{ route('admin.trains.index')}}">Поезда</a></li>
                                <li><i class=""></i><a href="{{ route('admin.countries.index')}}">Cтраны</a></li>
                                <li><i class=""></i><a href="{{ route('admin.wagons.index')}}">Вагоны</a></li>
                                <li><i class=""></i><a href="{{ route('admin.containers.index')}}">Контейнеры</a></li>
                                <li><i class=""></i><a href="{{ route('admin.stations.index')}}">Cтанций</a></li>
                                <li><i class=""></i><a href="{{ route('admin.directions.index')}}">Направления</a></li>
                        
                        </ul>
                    </li>
                @endif
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-shopping-cart"></i>Заказы </a>
                        
                        <ul class="sub-menu children dropdown-menu" @if(auth()->user()->hasRole(App\Role::AGENT)) style="width:220px !important;" @endif> 
                       
                            @if(auth()->user()->hasRole(App\Role::AGENT)||auth()->user()->hasRole(App\Role::DOSTAVKA))
                                <li><i class=""></i>
                                    <a href="{{route('admin.orders.index')}}">Все Заказы  
                                        <span class="badge badge-primary">
                                            @if(auth()->user()->hasRole(App\Role::DOSTAVKA)) {{ count(App\Order::whereIn('id',App\OrderStatus::agent(App\OrderStatus::AGENT)
                                        ->pluck('order_id')->toArray())->where('created_by',auth()->id())->latest('created_at')->get()) }} 
                                            @else  {{ count(App\Order::whereIn('id',App\OrderStatus::state(App\OrderStatus::AGENT)
                                        ->pluck('order_id'))->where('user_id', '!=', auth()->id())->get()) }}
                                            @endif
                                        </span>
                                    </a>
                                </li>
                            @endif    
                            @if(auth()->user()->hasRole(App\Role::AGENT))
                                <li><i class=""></i>
                                    <a href="{{route('admin.orders.my_orders.index')}}">Мои заказы
                                        <span class="badge badge-primary">{{ App\Order::whereIn('id',App\OrderStatus::state(App\OrderStatus::AGENT)
                                        ->pluck('order_id'))->where(['user_id' => auth()->id()])->get()->count()}}</span>
                                    </a>
                                </li>
                            @endif
                            @if(auth()->user()->hasRole(App\Role::AGENT))
                                <li><i class=""></i>
                                    <a href="{{route('admin.orders.closed.index')}}">
                                        Закрыто
                                        <span class="badge badge-primary">
                                        {{ App\Order::whereIn('id',App\OrderStatus::paid(App\OrderStatus::AGENT)
                                        ->pluck('order_id'))->get()->count()}}
                                        </span>  
                                    </a>
                                </li>
                            @endif
                            @if(auth()->user()->hasRole(App\Role::AGENT))
                                <li><i class=""></i>
                                    <a href="{{route('admin.orders.canceleds.index')}}">
                                        Отмененные заказы 
                                        <span class="badge badge-primary">{{ count(App\OrderStatus::where(['body' =>'Заказ Отменен!'])->get())}}
                                        </span>
                                    </a>
                                </li>
                            @endif
                            
                            @if(auth()->user()->hasRole(App\Role::POKUPATEL))
                                <li><i class=""></i>
                                    <a href="{{route('admin.orders.my_orders.index')}}">Мои заказы
                                        <span class="badge badge-primary">{{App\Order::whereIn('id',App\OrderStatus::state(App\OrderStatus::POKUPATEL)
                                        ->pluck('order_id'))->where(['user_id' => auth()->id()])->get()->count() }}
                                    </a>
                                </li>
                            @endif
                            
                        </ul>
                    </li>
                @if(auth()->user()->hasRole(App\Role::AGENT))
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-cog"></i>Настройки</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class=""></i><a href="{{ route('admin.roles.index')}}">Роли</a></li>
                            <li><i class=""></i><a href="{{ route('admin.permissions.index')}}">Разрешение</a></li>
                        
                        </ul>
                    </li>
                @endif
            </ul>
            
            <div class="user-area dropdown float-right">
                <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="user-avatar rounded-circle" src="{{ asset('admin/images/images.jpeg') }}" alt="User Avatar">
                    <span>{{Auth::user()->name}}</span>
                </a>

                <div class="user-menu dropdown-menu">
                    <a class="nav-link" href="{{URL::to('admin/profil/account')}}"><i class="fa fa-user"></i> Profil</a>
                    <a class="nav-link" href="{{URL::to('admin/logout')}}"><i class="fa fa-sign-in"></i> Выход</a>
                </div>
            </div>

        </div>
    </div>
</header>