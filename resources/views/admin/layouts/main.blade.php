<!DOCTYPE html>
<html class="no-js" lang="en"> 
<head>
    <title>Train Admin</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Train" />
    <meta name="keywords" content="Train" />
    <meta name="author" content="GoodOne" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" href="https://i.imgur.com/QRAUqs9.png">
    <link rel="shortcut icon" href="https://i.imgur.com/QRAUqs9.png">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
    <link rel="stylesheet" href="{{ asset('admin/css/cs-skin-elastic.css')}}" />
    <link rel="stylesheet" href="{{ URL::to('admin/toastr/css/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/datepicker.min.css')}}" />
    <link rel="stylesheet" href="{{ asset('admin/css/style.css')}}" />
    
    
    <style>
         .sub-menu li{
            margin-top: -23px;
              padding:0px !important;
            }
            .sub-menu .subtitle{
              display:none !important;
            }
            .btn-xs{
              padding:1px 5px !important;
            }
            .badge{
              padding:6px 6px !important;
            }
    </style>
    
    @yield('style')
    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
   
</head>

<body class="@yield('class')">
   
     @yield('content')
      <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="{{ asset('/admin/js/main.js')}}"></script>
    <script src="{{ asset('admin/toastr/js/toastr.min.js') }}"></script>
    {!! Toastr::message() !!}
   
    @yield('script')
   
</body>
</html>
