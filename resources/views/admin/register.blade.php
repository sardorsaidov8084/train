@extends('admin.layouts.main')

@section('style')

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <style>
        .login-form label {
            color:black;
            text-transform:none;   
        }
    </style>
@endsection
@section('class')
    bg-dark
@endsection
@section('content')
    <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <!-- <div class="login-logo">
                    <a href="index.html">
                        <img class="align-content" src="images/logo.png" alt="">
                    </a>
                </div> -->
                <div class="login-form">
                    <form action="{{ route('admin.register')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Имя пользователя</label>
                            <input type="text" class="form-control" name="name" placeholder="Имя пользователя">
                            @if($errors->has('name'))
                                <span style="font-size: 10px; color: red; float: left">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Телефон</label>
                            <input type="number" class="form-control" name ="phone" placeholder = "Телефон">
                            @if($errors->has('phone'))
                                <span style="font-size: 10px; color: red; float: left">{{ $errors->first('phone') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Эл. адрес</label>
                            <input type="email" class="form-control" name="email" placeholder="Эл. адрес">
                            @if($errors->has('email'))
                                <span style="font-size: 10px; color: red; float: left">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Пароль</label>
                            <input type="password" class="form-control" name="password" placeholder="Пароль">
                            @if($errors->has('password'))
                                <span style="font-size: 10px; color: red; float: left">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                       
                        <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">Регистрация</button>
                        <div class="register-link m-t-15 text-center">
                            <p>Уже есть аккаунт ? <a href="{{URL::to('admin/login')}}"> Войти в систему</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
