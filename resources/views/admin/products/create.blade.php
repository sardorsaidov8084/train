@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/lib/chosen/chosen.min.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <style>
    .chosen-container{
        margin-left:15px;
    }
    </style>
   
@endsection
@section('content')
<div id="right-panel" class="right-panel">

  @include('admin.includes.header')
  <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-5">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><strong><a href="{{route('admin.products.index')}}">Продукты</a></strong></li>
                                <li class="">Добавить </li>
                            </ol>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Форма элементов</strong> 
                        </div>
                        <div class="card-body">
                            <div class="card-body card-block">
                                <form action="{{ route('admin.products.store')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="name" class=" form-control-label">Имя</label>
                                                </div>
                                                <div class="col-12 col-sm-9">
                                                    <input type="text" id="name" name="name" placeholder="" class="form-control">
                                                    @if($errors->has('name'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('name') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="category_id" class=" form-control-label">Категория Имя</label>
                                                </div>
                                                <select data-placeholder="" class="standardSelect" name="category_id" tabindex="1">
                                                        @foreach($categories as $category)
                                                            <option value="{{ $category->id }}"> {{ $category->name }}</option>
                                                        @endforeach
                                                 </select>
                                                    @if($errors->has('category_id'))
                                                        <span style="font-size: 10px; color: red; float: left; margin-left:15px;">{{ $errors->first('category_id') }}</span>
                                                    @endif
                                            </div>
                                        </div>  
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="name" class=" form-control-label">Цена</label>
                                                </div>
                                                <div class="col-12 col-sm-9">
                                                    <input type="text" id="name" name="price" placeholder="" class="form-control">
                                                    @if($errors->has('price'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('price') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                       
                                        <div class="col-md-4">
                                            
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="company_id" class=" form-control-label">Компания Имя</label>
                                                </div>
                                                <select data-placeholder="" class="standardSelect" name="company_id" tabindex="1">
                                                        @foreach($companies as $company)
                                                            <option value="{{ $company->id }}"> {{ $company->name }}</option>
                                                        @endforeach
                                                 </select>
                                                    @if($errors->has('company_id'))
                                                        <span style="font-size: 10px; color: red; float: left; margin-left:15px;">{{ $errors->first('company_id') }}</span>
                                                    @endif
                                            </div>

                                            
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row form-group">
                                                        <div class="col-12 col-sm-9">
                                                            <label for="quantity" class=" form-control-label">Количество</label>
                                                        </div>
                                                        <div class="col-12 col-sm-9">
                                                            <input type="text" id="quantity" name="quantity" placeholder="" class="form-control">
                                                            @if($errors->has('quantity'))
                                                                <span style="font-size: 10px; color: red; float: left">{{ $errors->first('quantity') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="row form-group">
                                                        <div class="col-12 col-sm-9">
                                                            <label for="company_id" class=" form-control-label">Вес</label>
                                                        </div>
                                                        <select data-placeholder="" class="form-control" name="weight" tabindex="1">
                                                                
                                                            <option value="кг">КГ</option>
                                                            <option value="т">Т</option>
                                                        </select>
                                                            @if($errors->has('weight'))
                                                                <span style="font-size: 10px; color: red; float: left; margin-left:15px;">{{ $errors->first('weight') }}</span>
                                                            @endif
                                                    </div>
                                                </div>
                                            </div>
                                           
                                        </div>
                                       
                                        <div class="col-md-6">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="image" class=" form-control-label">Фотографии</label>
                                                </div>
                                                <div class="col-12 col-sm-9">
                                                    <input type="file" id="file-input" name ="image" >
                                                    @if($errors->has('image'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('image') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <div class="form-check">
                                                        <div class="checkbox">
                                                            <label for="checkbox1" class="form-check-label ">
                                                                <input type="checkbox" id="checkbox1" name="status" value="" class="form-check-input">Статус
                                                            </label>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="row form-group">
                                                
                                                <div class="col-12 col-sm-9">
                                                    <textarea type="text" id="description" name="description" rows="6" placeholder="описание" class="form-control"></textarea>
                                                    @if($errors->has('description'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('description') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div> 
                                        
                                        
                                    </div>
                                    <br>
                                    <div class="row form-group">
                                        <div class="col-12 col-sm-9">
                                            <button type="submit" class="btn btn-primary">Сохранить</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>                     
                </div>
            </div>
            
        </div><!-- .animated -->
    </div><!-- .content -->

    <div class="clearfix"></div>

</div><!-- /#right-panel -->

@endsection
@section('script')
<script src="{{ asset('admin/js/lib/chosen/chosen.jquery.min.js')}}"></script>

<script>
    jQuery(document).ready(function() {
        jQuery(".standardSelect").chosen({
            disable_search_threshold: 10,
            no_results_text: "Oops, nothing found!",
            width: "100%"
        });
    });
</script>
@endsection
