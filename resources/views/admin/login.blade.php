@extends('admin.layouts.main')

@section('style')

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <style>
        .login-form label {
            color:black;
            text-transform:none;   
        }
    </style>
@endsection
@section('class')
    bg-dark
@endsection
@section('content')
<div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <!-- <div class="login-logo">
                    <a href="index.html">
                        <img class="align-content" src="{{ asset('admin/images/logo.png') }}" alt="">
                    </a>
                </div> -->
                <div class="login-form">
                    <form action="{{ route('admin.login')}}" method="post" enctype="multipart/form-data" class="">
                        @csrf
                        <div class="form-group">
                            <label>Телефон администратора :  || <a href="{{URL::to('user/contact')}}">Контакт</a></label>
                        </div>
                        <div class="form-group">
                            <label>Адрес электронной почты</label>
                            <input type="email" class="form-control" name="email" placeholder="Эл. почта">
                           
                        </div>
                        <div class="form-group">
                            <label>Пароль</label>
                            <input type="password" class="form-control" name="password"placeholder="Пароль">
                        </div>
                        <div class="checkbox">
                            <!-- <label>
                                <input type="checkbox" name=""> Remember Me
                            </label> -->
                            <!-- <label class="pull-right">
                                <a href="#">Забытый пароль?</a>
                            </label> -->

                        </div>
                        <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Вход</button>
                        
                        <div class="register-link m-t-15 text-center">
                            <p>Нет аккаунта ? <a href="{{URL::to('admin/register')}}"> Подпишите здесь</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

