@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/lib/chosen/chosen.min.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <style>
        .chosen-container{
            margin-left:15px;
            width:83% !important;
        }
    </style>
   
@endsection
@section('content')
<div id="right-panel" class="right-panel">
        
  @include('admin.includes.header')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-5">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><strong><a href="{{route('admin.directions.index')}}">Направления</a></strong></li>
                                <li class="">Обновить</li>
                            </ol>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Форма элементов</strong> 
                        </div>
                        <div class="card-body">
                            <div class="card-body card-block">
                                <form action="{{ route('admin.directions.update',['direction' => $direction->id])}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    @csrf
                                    @method('PUT')
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="name" class=" form-control-label">Имя</label>
                                                </div>
                                                <div class="col-12 col-sm-9">
                                                    <input type="text" id="name" name="name" placeholder="Имя" class="form-control" value="{{$direction->name}}">
                                                    @if($errors->has('name'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('name') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                             <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="name" class=" form-control-label">Поезда</label>
                                                </div>
                                                <div class="col-12 col-sm-9">
                                                    <select class="form-control" name="train_id">
                                                       @foreach(App\Train::get() as $train)
                                                        <option value="{{$train->id}}" >{{$train->name}}</option>
                                                       @endforeach
                                                    </select>
                                                    @if($errors->has('train_id'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('train_id') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="row">
                                        <div class="col-md-7">

                                            <div class="col-12 col-sm-9 row">
                                                <label for="name" class=" form-control-label">Ведите остановки и дни</label>
                                            </div> 
                                            @php $count=0; @endphp
                                            @foreach(App\StationTime::where(['direction_id' => $direction->id ])->get() as $time)
                                               @php  $count++;@endphp
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        @if($direction->stations)
                                                            <select name="station[{{$count}}][id]" class="form-control">
                                                                <option value=""></option>
                                                                @foreach(App\Station::get() as $station)
                                                                <option value="{{$station->id}}" @if($time->station_to == $station->id )selected @endif>{{$station->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <br>
                                                        @endif   
                                                    </div>
                                                    <div class="col-md-2">
                                                        <input name="station[{{$count}}][day]" type="text" value="{{$time->time}}" class="form-control" >
                                                    </div>
                                                </div>
                                           @endforeach
                                        </div>
                                   </div>
                                    <br>
                                    <div class="row form-group">
                                        <div class="col-12 col-sm-9">
                                            <button type="submit" class="btn btn-primary">Сохранить</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>                     
                </div>
            </div>
            
        </div><!-- .animated -->
    </div><!-- .content -->

    <div class="clearfix"></div>

</div><!-- /#right-panel -->

@endsection
@section('script')
<script src="{{ asset('admin/js/lib/chosen/chosen.jquery.min.js')}}"></script>

<script>
    jQuery(document).ready(function() {
        jQuery(".standardSelect").chosen({
            disable_search_threshold: 10,
            no_results_text: "Oops, nothing found!",
            width: "100%"
        });
    });
</script>
@endsection
