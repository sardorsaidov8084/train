@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/lib/datatable/dataTables.bootstrap.min.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <style>
        .chosen-container{
            margin-left:15px;
            width:83% !important;
        }
      
    </style>
   
@endsection
@section('content')
<div id="right-panel" class="right-panel">
        
  @include('admin.includes.header')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-5">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><strong><a href="{{route('admin.directions.index')}}">Направления</a></strong></li>
                                <li class="">Добавить </li>
                            </ol>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Форма элементов</strong> 
                        </div>
                        <div class="card-body">
                            <div class="card-body card-block">
                                <form action="{{ route('admin.directions.store')}}" method="post" enctype="multipart/form-data" class="form-horizontal" id="direction_id">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="name" class=" form-control-label">Имя</label>
                                                </div>
                                                <div class="col-12 col-sm-9">
                                                    <input type="text" name="name" placeholder="" class="form-control">
                                                    @if($errors->has('name'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('name') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                             <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="name" class=" form-control-label">Поезда</label>
                                                </div>
                                                <div class="col-12 col-sm-9">
                                                    <select class="form-control" name="train_id">
                                                       @foreach(App\Train::get() as $train)
                                                        <option value="{{$train->id}}">{{$train->name}}</option>
                                                       @endforeach
                                                    </select>
                                                    @if($errors->has('train_id'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('train_id') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                                
                                                    <div class="col-12 col-sm-9">
                                                        <label for="name" class=" row form-control-label">Ведите остановки и дни</label>
                                                    </div>
                                               
                                            <div id="category_attribut_rows">
                                            
                                            </div>
                                            <div class="col-md-3"></div>
                                            <div class="col-md-4">
                                                <div class="row">
                                                    <div class=" row form-group">
                                                        
                                                        <div class="col-12 col-sm-9">
                                                            <span id="addAttribute" class="btn btn-info" >Добавить Cтанций</span>
                                                        </div>
                                                    </div>
                                                </div>        
                                            </div>
                                        </div>
                                    </div>
                                   <div class="row form-group" style="float:right">
                                        <div class="col-12 col-sm-9">
                                            <button type="submit" class="btn btn-primary">Сохранить</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>                     
                </div>
            </div>
            
        </div><!-- .animated -->
    </div><!-- .content -->

    <div class="clearfix"></div>

</div><!-- /#right-panel -->

@endsection
@section('script')
    <script src="{{ asset('admin/js/lib/data-table/datatables.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/jszip.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/vfs_fonts.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.print.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('admin/js/init/datatables-init.js') }}"></script>
    <script src="{{ asset('/admin/js/datepicker.min.js')}}"></script>
    <script src="{{ asset('admin/js/i18n/datepicker.en.js') }}"></script>
<script>
var start = new Date(),
        prevDay,
        startHours = 9;

    // 09:00 AM
    start.setHours(9);
    start.setMinutes(0);

    // If today is Saturday or Sunday set 10:00 AM
    if ([6, 0].indexOf(start.getDay()) != -1) {
        start.setHours(10);
        startHours = 10
    }
    function reloadTimePicker() {
        $('.timepicker').datepicker({
        timepicker: true,
        language: 'ru',
        startDate: start,
        minHours: startHours,
        maxHours: 18,
        onSelect: function (fd, d, picker) {
            // Do nothing if selection was cleared
            if (!d) return;

            var day = d.getDay();

            // Trigger only if date is changed
            if (prevDay != undefined && prevDay == day) return;
            prevDay = day;

            // If chosen day is Saturday or Sunday when set
            // hour value for weekends, else restore defaults
            if (day == 6 || day == 0) {
                picker.update({
                    minHours: 10,
                    maxHours: 16
                })
            } else {
                picker.update({
                    minHours: 9,
                    maxHours: 18
                })
            }
        }
    });
    }
  

</script>
<script>
   
    
    let stationsGet = function(callback){
        var data;
        jQuery.ajax({
                    url: "{{ route('stations.select')}}",
                    type:'get',
                    async: false,
         success: function(results){
            text = "";        

             for(var i =0; i < results.length; i++){
                 text = text + '<option value=\"' + results[i].id + '\">' + results[i].name + '</option>';
             }   
             data = text;
  
        }});
        return data;
    }
    let counter = 0;
    jQuery("#addAttribute").click(function () {
                counter++;                
                    let row = "<div class=\"row\" id=\""+counter+"\" style=\"margin-bottom:20px !important;\">\n" +
                   
                     "<div class=\"col-md-6\">\n" +
                      "<select name=\"station["+counter+"][id]\" required class=\"form-control form-control-primary\" onchange=\"changeInputTypeBySelect(this)\">\n" +
                   
                    stationsGet()                        
                    + "</select>\n" +
                    "</div>\n" +
                    "<div class=\"col-md-4\">\n" +
                    "<input class=\"form-control \" type=\"text\" name=\"station["+counter+"][day]\"  required  >\n" +
                    "</div>\n" +
                    "<div col-md-2>"+
                        "<span onclick='removeThis(this)' class=\"delete btn btn-danger \" data-id="+counter+"><i class=\"fa fa-trash\"></i></span>\n" +
                     "</div>"+
                    "</div>\n";
                
                jQuery('#category_attribut_rows').append(row);
                reloadTimePicker();
            });

            function removeThis(e) {
                jQuery("#" + jQuery(e).data('id')).remove();
               
            }

            
            function changeInputTypeBySelect(e){
                let val =jQuery(e).val();
                let $element =jQuery(e).closest("div.row").find("input[placeholder=value]");

                jQuery($element).attr('type',val);
            }
        
</script>

@endsection
