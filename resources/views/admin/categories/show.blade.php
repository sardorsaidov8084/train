@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/lib/datatable/dataTables.bootstrap.min.css') }}">
    
@endsection
@section('content')
    <div id="right-panel" class="right-panel">
        @include('admin.includes.header')
        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                 <div class="row m-0">    
                    <div class="col-sm-8">
                        <div class="page-header float-left">
                            <div class="page-title">
                            <ol class="breadcrumb text-right">

                                <li class="">Таблица</li>
                            </ol>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="page-header float-right">
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Таблица данных</strong>
                            </div>
                            <div class="table-stats order-table ov-h">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            <th>Изображение</th>
                                            <th>Имя</th>
                                            <th>Описание</th>
                                            <th>Настройка</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>@if($category->image)<img src="{{ URL::to($category->image->path)}}" alt="prod img" class="img-fluid" style="max-width: 90px;"> @else no image @endif </td> 
                                        <td>{{ $category->name }}</td>
                                        <td>{{ $category->description }}</td>
                                                <td class="text-center">
                                                    <a class="btn btn-primary btn-xs" href="{{route('admin.categories.edit',['category'=>$category->id])}}"><i class="fa fa-pencil"></i></a>
                                                
                                                    <form action="{{route('admin.categories.destroy',['category'=>$category->id])}}" method="post" style="display:inline-block">
                                                        @csrf
                                                        @method('delete')
                                                        <button class="btn btn-danger btn-xs"  onclick="return confirm('Вы действительно хотите удалить его?')"  type="submit"><i class="fa fa-trash"></i></button>
                                                    </form> 
                                
                                        </td>      
                                      </tr>
                                        
                                    </tbody>
                                </table>
                               
                            </div> 
                            <!-- /.table-stats -->
                        </div>
                        
                       
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Таблица данных</strong>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Изображение</th>
                                            <th>Имя</th>
                                            <th>Категория Имя</th>
                                            <th>Компания Имя</th>
                                            <th>Цена</th>
                                            <th>Количество</th>
                                            <th>Настройка</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($products as $product)
                                            <tr>
                                                
                                                <td>@if($product->image)<img src="{{ URL::to($product->image->path)}}" alt="prod img" class="img-fluid" style="max-width: 90px;"> @else no image @endif </td>
                                                <td>{{$product->name}}</td>
                                                <td>
                                                    @if($product->category) {{$product->category->name}} 
                                                    @else no Category
                                                    @endif
                                                </td>
                                                <td>{{ $product->company->name }}</td>
                                                <td>{{$product->price}}</td>
                                                <td>{{$product->quantity}}</td>
                                                    <td class="text-center">
                                                

                                                        <a class="btn btn-primary btn-xs" href="{{route('admin.products.edit',['id'=>$product->id])}}"><i class="fa fa-pencil"></i></a>
                                                    
                                                    
                                                        <form action="{{route('admin.products.destroy',['product'=>$product->id])}}" method="post" style="display:inline-block">
                                                            @csrf
                                                            @method('delete')
                                                            <button class="btn btn-danger btn-xs"  onclick="return confirm('Вы действительно хотите удалить его?')"  type="submit"><i class="fa fa-trash"></i></button>
                                                        </form> 
                                                
                            
                              </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    

                </div>
                
            </div><!-- .animated -->

                
        </div>
       
        <div class="clearfix"></div>

      
    </div>
@endsection
@section('script')
    <script src="{{ asset('admin/js/lib/data-table/datatables.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/jszip.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/vfs_fonts.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.print.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('admin/js/init/datatables-init.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
         
      } );
    
</script>
    
@endsection
