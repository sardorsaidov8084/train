@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/lib/chosen/chosen.min.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <style>
    .chosen-container{
        margin-left:15px;
    }
    </style>
@endsection
@section('content')
<div id="right-panel" class="right-panel">

  @include('admin.includes.header')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-5">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><strong><a href="{{route('admin.categories.index')}}">Категории</a></strong></li>
                                <li class="">Обновить</li>
                            </ol>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                             <strong>Форма элементов</strong> 
                        </div>
                        <div class="card-body">
                            <div class="card-body card-block">
                                <form action="{{ route('admin.categories.update',['category' =>$category->id]) }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    @csrf
                                    @method('PUT')
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="name" class=" form-control-label">Имя</label>
                                                </div>
                                                <div class="col-12 col-sm-9">
                                                    <input type="text" id="name" name="name" placeholder="Name" class="form-control" value="{{$category->name}}">
                                                    @if($errors->has('name'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('name') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="parent_id" class=" form-control-label">Какая категория принадлежит</label>
                                                </div>
                                                <select data-placeholder="" class="standardSelect" tabindex="1" name="parent_id">
                                                    <option value="0" label="default" >...</option>
                                                    @foreach(App\Category::latest()->get() as $select)
                                                        <option value="{{ $category->id }}" @if($select->id == $category->id)selected @endif> {{ $category->name }}</option>
                                                    @endforeach   
                                                </select>                       
                                            </div>
                                        </div>  
                                    </div>
                                    
                                   <div class="row">
                                        <div class="col-md-6">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="image" class=" form-control-label">Фотографии</label>
                                                    <br>
                                                    @if($category->image)
                                                        <img src="{{ URL::to($category->image->path)}}" alt="prod img" class="img-fluid" style="max-height:100px;">
                                                    @endif
                                                </div>
                                                
                                                <div class="col-12 col-sm-9">
                                                <br>
                                                    <input type="file" id="file-input" name ="image" >
                                                    @if($errors->has('image'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('image') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <div class="">
                                                        <div class="checkbox">
                                                            <label for="checkbox1" class="form-control-label ">Статус</label>
                                                           <br>
                                                            <input type="checkbox" id="checkbox1" name="status" value="" class="form-control-label" @if($category->status==1) checked @endif>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="row form-group">
                                                
                                                <div class="col-12 col-sm-9">
                                                    <textarea type="text" id="description" name="description" rows="6" placeholder="описание" class="form-control" >{{$category->description}}</textarea>
                                                    @if($errors->has('description'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('description') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                    <br>
                                    <div class="row form-group">
                                        <div class="col-12 col-sm-9">
                                            <button type="submit" class="btn btn-primary">Добавить</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>                     
                </div>
            </div>
            
        </div><!-- .animated -->
    </div><!-- .content -->

    <div class="clearfix"></div>

</div><!-- /#right-panel -->

@endsection
@section('script')
<script src="{{ asset('admin/js/lib/chosen/chosen.jquery.min.js')}}"></script>

<script>
    jQuery(document).ready(function() {
        jQuery(".standardSelect").chosen({
            disable_search_threshold: 10,
            no_results_text: "Oops, nothing found!",
            width: "100%"
        });
    });
</script>
@endsection
