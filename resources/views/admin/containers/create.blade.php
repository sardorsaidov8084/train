@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/lib/chosen/chosen.min.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <style>
        .chosen-container{
            /* margin-left:15px; */
            width:83% !important;
        }
    </style>
   
@endsection
@section('content')
<div id="right-panel" class="right-panel">
        
  @include('admin.includes.header')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-5">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><strong><a href="{{route('admin.containers.index')}}">Контейнеры</a></strong></li>
                                <li class="">Добавить </li>
                            </ol>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <!-- <strong>Basic Form</strong> Elements -->
                        </div>
                        <div class="card-body">
                            <div class="card-body card-block">
                                <form action="{{ route('admin.containers.store')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="name" class=" form-control-label">Имя</label>
                                                </div>
                                                <div class="col-12 col-sm-9">
                                                    <input type="text" id="name" name="name" placeholder="Имя" class="form-control">
                                                    @if($errors->has('name'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('name') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="order_id" class=" form-control-label">Ключ заказа</label>
                                                </div>
                                                <select data-placeholder="" class="standardSelect" name ="order_id"tabindex="1">
                                                        <option value="">...</option>
                                                        @foreach(App\Order::latest()->get() as $order)
                                                            <option value="{{ $order->id }}"> {{ $order->id }}</option>
                                                        @endforeach
                                                 </select>
                                                    @if($errors->has('order_id'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('order_id') }}</span>
                                                    @endif
                                            </div>
                                        </div>    
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="image" class=" form-control-label">Фотографии</label>
                                                </div>
                                                <div class="col-12 col-sm-9">
                                                    <input type="file" id="file-input" name ="image" >
                                                    @if($errors->has('image'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('image') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <label for="wagon_id" class=" form-control-label">Ключ вагона</label>
                                                </div>
                                                <select data-placeholder="" class="standardSelect" name ="wagon_id"tabindex="1">
                                                        <option value="">...</option>
                                                        @foreach(App\Wagon::latest()->get() as $wagon)
                                                            <option value="{{ $wagon->id }}"> {{ $wagon->id }}</option>
                                                        @endforeach
                                                 </select>
                                                    @if($errors->has('wagon_id'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('wagon_id') }}</span>
                                                    @endif
                                            </div>
                                        </div>    
                                    </div>
                                    <br>
                                    <div class="row form-group">
                                        <div class="col-12 col-sm-9">
                                            <button type="submit" class="btn btn-primary">Сохранить</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>                     
                </div>
            </div>
            
        </div><!-- .animated -->
    </div><!-- .content -->

    <div class="clearfix"></div>

</div><!-- /#right-panel -->

@endsection
@section('script')
<script src="{{ asset('admin/js/lib/chosen/chosen.jquery.min.js')}}"></script>

<script>
    jQuery(document).ready(function() {
        jQuery(".standardSelect").chosen({
            disable_search_threshold: 10,
            no_results_text: "Oops, nothing found!",
            width: "100%"
        });
    });
</script>
@endsection
