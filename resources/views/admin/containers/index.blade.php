@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/lib/datatable/dataTables.bootstrap.min.css') }}">
    
@endsection
@section('content')
    <div id="right-panel" class="right-panel">
        @include('admin.includes.header')
        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                 <div class="row m-0">    
                    <div class="col-sm-8">
                        <div class="page-header float-left">
                            <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><strong><a href="#">Контейнеры</a></strong></li>
                                <li class="">Таблица</li>
                            </ol>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <a href="{{ route('admin.containers.create') }}" class="btn btn-primary btn-square" style="float: left;margin-top:7px;"><i class="ti-plus"></i> Добавить  </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Таблица данных</strong>
                            </div>
                            <div class="card-body">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Изображение</th>
                                            <th>Имя</th>
                                            <th>Ключ заказа</th>
                                            <th>Ключ вагона</th>
                                            <th>Settings</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($containers as $container)
                                        <tr>
                                            <td> 
                                                @if($container->image)  
                                                <img src="{{ URL::to($container->image->path)}}" alt="prod img" class="img-fluid" style="max-width: 90px;float:center;">
                                                    @else no img
                                                @endif 
                                            </td>
                                            <td>{{$container->name}}</td>
                                            <td>
                                                 {{$container->order_id}}
                                                
                                            </td>
                                            <td>{{$container->wagon_id}}</td>
                                            <td class="text-center">
                                                <a class="btn btn-primary btn-xs" href="{{route('admin.containers.edit',['container'=>$container->id])}}"><i class="fa fa-pencil"></i></a>
                                    
                                                <form action="{{route('admin.containers.destroy',['container'=>$container->id])}}" method="post" style="display:inline-block">
                                                    @csrf
                                                    @method('delete')
                                                    <button class="btn btn-danger btn-xs"  onclick="return confirm('Вы действительно хотите удалить его?')"  type="submit"><i class="fa fa-trash"></i></button>
                                                </form>	
											</td>
                                            
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>                             
                            </div>
                        </div>
                    </div>


                </div>
            </div><!-- .animated -->
        </div>
       
        <div class="clearfix"></div>

        <!-- @include('admin.includes.footer') -->
    </div>
@endsection
@section('script')
    <script src="{{ asset('admin/js/lib/data-table/datatables.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/jszip.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/vfs_fonts.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.print.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('admin/js/init/datatables-init.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
      } );
    </script>
    
@endsection
