@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/lib/chosen/chosen.min.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <style>
    .chosen-container{
        margin-left:15px;
    }
    </style>
@endsection
@section('content')
<div id="right-panel" class="right-panel">

  @include('admin.includes.header')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-5">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><strong><a href="{{route('admin.roles.index')}}">Роли</a></strong></li>
                                <li class="">вид </li>
                            </ol>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Роль</strong> 
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="home7" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h2><strong>{{ $role->name }}</strong></h2>
                                                </div>
                                            </div>
                                            <br>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h4>
                                                        {{ $role->description }}
                                                    </h4>
                                                </div>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="fontawesome-icon-list">
                                 <section id="web-application">
                                    <h4 style=""><strong>Разрешение</strong></h4>
                                        <br>
                                    <div class="row fontawesome-icon-list">
                                    @foreach($role->permissions as $permission)
                                        <div class="fa-hover col-lg-4 col-md-6"> 
                                            <div class="form-check">
                                                <label class="form-check-label" style="margin-bottom:5px;">{{ $permission->name }}</label>
                                            </div>
                                        </div>
                                    @endforeach
                                        
                                    </div>

                                </section>
                            </div>

                        </div>
                    </div>                     
                </div>
            </div>
            
        </div><!-- .animated -->
    </div><!-- .content -->

    <div class="clearfix"></div>

</div><!-- /#right-panel -->

@endsection
@section('script')
<script src="{{ asset('admin/js/lib/chosen/chosen.jquery.min.js')}}"></script>

<script>
    jQuery(document).ready(function() {
        jQuery(".standardSelect").chosen({
            disable_search_threshold: 10,
            no_results_text: "Oops, nothing found!",
            width: "100%"
        });
    });
</script>
@endsection
