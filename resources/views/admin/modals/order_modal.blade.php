<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#cancelOrder" data-whatever="@getbootstrap">Отменить заказ</button>

<div class="modal fade" id="cancelOrder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <form id="formCanceled" action="{{URL::to('admin/orders/my_orders/delete')}}" method="POST">
        @csrf
            <div class="modal-header bg-c-danger" style="background:red;">
            <div class="row">
                <div class="col-md-8"><h5 class="modal-title" id="exampleModalLabel">Причина</h5></div>
                <div class="col-md-4"><button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-bottom:-15px;">
                <span aria-hidden="true">&times;</span>
            </button></div>
            </div>
            
            
            </div>
            <div class="modal-body" >
                
                <input type="hidden" name="order_id" value="{{ $order->id }}">
                <div class="form-group">
                <label for="message-text" class="col-form-label">Причина:</label>
                <textarea name="description" class="form-control" minlength="5" required id="message-text" placeholder="Причина"></textarea>
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">отмена</button>
            <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </form>
        </div>
    </div>
</div>