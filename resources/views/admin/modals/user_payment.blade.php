<button type="button" class="btn btn-success" data-toggle="modal" data-target="#paymentUser" data-whatever="@getbootstrap">Уплатить</button>

<div class="modal fade" id="paymentUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <form id="formCanceled" action="{{ route('admin.transactions.user_payment') }}" method="post" >
        @csrf
            <div class="modal-header bg-info" style="">
            <div class="row">
                <div class="col-md-8"><h5 class="modal-title" id="exampleModalLabel">Cумма денег</h5></div>
                <div class="col-md-4"><button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-bottom:-15px;">
                <span aria-hidden="true">&times;</span>
            </button></div>
            </div>
            
            
            </div>
            <div class="modal-body" >
                
                <div class="form-group">
               
                <input type="hidden" name="user_id" value="{{ $user->id }}">
                <input type="number" name="payment"  class="form-control" required id="message-text">
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">отмена</button>
            <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </form>
        </div>
    </div>
</div>