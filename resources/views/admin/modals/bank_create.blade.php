<button type="button" class="btn btn-success" data-toggle="modal" data-target="#banksOrder" data-whatever="@getbootstrap">Avanis berish</button>

<div class="modal fade" id="banksOrder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <form id="formCanceled" action="{{ route('admin.banks.create.store') }}" method="post" >
        @csrf
            <div class="modal-header bg-info" style="">
            <div class="row">
                <div class="col-md-8"><h5 class="modal-title" id="exampleModalLabel">Qancha avans o'tkazmoqchisiz</h5></div>
                <div class="col-md-4"><button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-bottom:-15px;">
                <span aria-hidden="true">&times;</span>
            </button></div>
            </div>
            
            
            </div>
            <div class="modal-body" >
                
                <div class="form-group">
                <label for="message-text" class="col-form-label">
                    Kimga avans berishingizni tanlang
                </label>
                <select name="user_id" class="form-control">
                    @foreach($users as $user)
                        @if($user->company&&$user->id!=auth()->id())
                            <option value="{{$user->id}}">{{ $user->name  }} -- {{ $user->company['name'] }}</option>
                        @endif
                    @endforeach
                </select>
                <br>
                <input type="number" name="payment"  class="form-control" required id="message-text">
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">отмена</button>
            <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </form>
        </div>
    </div>
</div>