<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#{{$product->name}}" data-whatever="@getbootstrap"><span class="fa fa-plus"></span></button>
<div class="modal fade" id="{{$product->name}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <form id="formCanceled" action="{{URL::to('admin/stores/add')}}" method="POST">
        @csrf
            <div class="modal-header bg-c-danger" style="background:white;">
            <div class="row">
                <div class="col-md-8"><h4 class="modal-title" id="exampleModalLabel"><strong> Добавить продукты </strong></h4></div>
                <div class="col-md-4"><button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-bottom:-15px;">
                <span aria-hidden="true">&times;</span>
            </button></div>
            </div>
            
            
            </div>
            <div class="modal-body" >
                <div class="row">
                    <div class="col-md-12">
                        <h4><strong>Mahsulot nomi: </strong>{{$product->name}}</h4>
                    </div>
                </div>
                <input type="hidden" name="product_id" value="{{ $product->id }}">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Цена:</label>
                            <input type="number" name="price" class="form-control" value="{{$product->price}}" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Количество:</label>
                            <input type="number" name="quantity" class="form-control" required>
                        </div>
                    </div>
                </div>
                
                
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </form>
        </div>
    </div>
</div>