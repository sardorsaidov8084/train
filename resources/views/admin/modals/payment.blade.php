<button type="button" class="btn btn-success" data-toggle="modal" data-target="#paymentOrder" data-whatever="@getbootstrap">Уплатить</button>

<div class="modal fade" id="paymentOrder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <form id="formCanceled" action="{{ route('admin.transactions.store') }}" method="post" >
        @csrf
            <div class="modal-header bg-info" style="">
            <div class="row">
                <div class="col-md-8">
                    <h5 class="modal-title" id="exampleModalLabel">Cумма денег</h5>
                </div>
                <div class="col-md-4">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-bottom:-15px;">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            
            
            </div>
            <div class="modal-body" >
                
                <input type="hidden" name="order_id" value="{{ $order->id }}">
                <div class="form-group">
                <label for="message-text1" class="col-form-label">
                    @if($order->transactionAmount($order->id))    
                         Jami summa : {{ $order->amount  }} UZB --- Qolgan summa : {{ $order->amount-$order->transactionAmount($order->id) }} UZB
                    @else
                         Jami summa : {{ $order->amount  }} UZB 
                    @endif
                </label>
                <input type="hidden" name="order_id" value="{{ $order->id }}">
                <input type="number" name="payment"  class="form-control" required id="message-text1">
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">отмена</button>
            <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </form>
        </div>
    </div>
</div>