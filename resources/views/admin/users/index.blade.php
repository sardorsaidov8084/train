@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/lib/datatable/dataTables.bootstrap.min.css') }}">
    
@endsection
@section('content')
    <div id="right-panel" class="right-panel">
        @include('admin.includes.header')
        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                 <div class="row m-0">    
                    <div class="col-sm-8">
                        <div class="page-header float-left">
                            <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><strong>Данные пользователя</strong>
								 <li class="">Таблица</li>
                            </ol>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <a href="{{ route('admin.users.create') }}" class="btn btn-primary btn-square" style="float: left;margin-top:7px;"><i class="ti-plus"></i> Добавить  </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Таблица данных</strong>
                            </div>
                            <div class="card-body">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                        <th>№</th>
                                        <th>Ключ</th>
                                        <th>ФИО</th>
                                        <th>Роли</th>
                                        <th>Телефон</th>
                                        <!-- <th>Статус</th> -->
                                        <th>Время регистрации</th>
                                        {{-- <th>Чат</th> --}}
                                        <th>Настройка</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @php($inc=1)
												@foreach($users as $user)
												<tr>
													<td>{{ $inc++ }}</td>
													<td>#{{ $user->id }}</td>
													<td>{{ $user->name }}</td>
													<!-- <td></td>
													<td></td> -->
													<td>
														<ul style="list-style:none;">
                                                            @if($user->roles)
                                                                @foreach($user->roles as $role)
                                                                    <li>{{$role->name}}</li>
                                                                @endforeach
                                                            @endif    
														</ul>

													</td>
													<td>{{ $user->phone }}</td>
													
													<td>{{ $user->created_at }}</td>
													{{--<td>
														<button data-user_id="{{$user->id}}" class="btn btn-comment startChat">Начать чат</button>
													</td>--}}
                                            
                                            <td class="text-center">
                                           
                                                <a class="btn btn-primary btn-xs" href="{{route('admin.users.edit',['id'=>$user->id])}}"><i class="fa fa-pencil"></i></a>
                                                 <form action="{{route('admin.users.destroy',['id'=>$user->id])}}" method="post" style="display:inline-block">
                                                    @csrf
                                                    @method('delete')
                                                    <button class="btn btn-danger btn-xs"  onclick="return confirm('Вы действительно хотите удалить его?')"  type="submit"><i class="fa fa-trash"></i></button>
                                                </form>	
														
											</td>
                                            
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>                             
                            </div>
                        </div>
                    </div>


                </div>
            </div><!-- .animated -->
        </div>
       
        <div class="clearfix"></div>

        <!-- @include('admin.includes.footer') -->
    </div>
@endsection
@section('script')
    <script src="{{ asset('admin/js/lib/data-table/datatables.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/jszip.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/vfs_fonts.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.print.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('admin/js/init/datatables-init.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
      } );
    </script>
    
@endsection
