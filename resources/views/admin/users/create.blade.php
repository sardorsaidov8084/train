@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/lib/chosen/chosen.min.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <style>
    .chosen-container{
        margin-left:15px;
    }
    </style>
@endsection
@section('content')
<div id="right-panel" class="right-panel">

  @include('admin.includes.header')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-5">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><strong><a href="{{route('admin.users.index')}}">пользователь</a></strong></li>
                                <li class="">Добавить </li>
                            </ol>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-7 ">
                    <div class="card">
                        <div class="card-header">
                            <strong>Форма регистрации</strong> 
                        </div>
                        <div class="card-body">
                            <div class="card-body card-block">
                                <form action="{{ route('admin.users.store')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    @csrf
                                  
                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class=" form-control-label">Имя</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="name">
                                                    <div class="input-group-addon"><i class="fa fa-male"></i></div>
                                                    </div>  
                                                @if($errors->has('name'))
                                                    <span style="font-size: 10px; color: red; float: left">{{ $errors->first('name') }}</span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label class=" form-control-label">Эл. адрес</label>
                                                <div class="input-group">
                                                    <input type="email" class="form-control" name="email">
                                                    <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                                </div>  
                                                    @if($errors->has('email'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('email') }}</span>
                                                    @endif
                                            </div>
                                            <div class="form-group">
                                                <label class=" form-control-label">Телефон</label>
                                                <div class="input-group">
                                                    <input type="text" name="phone" class="form-control">
                                                    <div class="input-group-addon"><i class="fa fa-phone"></i></div>
                                                </div>  
                                                    @if($errors->has('phone'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('phone') }}</span>
                                                    @endif
                                            </div>
                                            <div class="form-group">
                                                <label class=" form-control-label">Пароль</label>
                                                <div class="input-group">
                                                    <input type="password" name="password" class="form-control">
                                                    <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                                                </div>  
                                                    @if($errors->has('name'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('name') }}</span>
                                                    @endif
                                            </div>
                                            <br>
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9" style="float:right;">
                                                    <button type="submit" class="btn btn-primary">Добавить</button>
                                                </div>
                                            </div>
                                            <div class="col-md-3"></div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>                     
                </div>
            </div>
            
        </div><!-- .animated -->
    </div><!-- .content -->

    <div class="clearfix"></div>

</div><!-- /#right-panel -->

@endsection
@section('script')
<script src="{{ asset('admin/js/lib/chosen/chosen.jquery.min.js')}}"></script>

<script>
    jQuery(document).ready(function() {
        jQuery(".standardSelect").chosen({
            disable_search_threshold: 10,
            no_results_text: "Oops, nothing found!",
            width: "100%"
        });
    });
</script>
@endsection
