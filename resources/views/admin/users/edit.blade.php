@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/lib/chosen/chosen.min.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <style>
    .chosen-container{
        margin-left:15px;
    }
    </style>
@endsection
@section('content')
<div id="right-panel" class="right-panel">

  @include('admin.includes.header')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-5">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><strong><a href="{{route('admin.users.index')}}">пользователь</a></strong></li>
                                <li class="">Обновить </li>
                            </ol>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-7 ">
                    <div class="card">
                        <div class="card-header">
                            <strong>Форма регистрации</strong> 
                        </div>
                        <div class="card-body">
                            <div class="card-body card-block">
                                <form action="{{ route('admin.users.update',['id' =>$user->id])}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    @csrf
                                    @method('PUT')  
                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class=" form-control-label">Имя</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="name" value="{{$user->name}}">
                                                    <div class="input-group-addon"><i class="fa fa-male"></i></div>
                                                    </div>  
                                                @if($errors->has('name'))
                                                    <span style="font-size: 10px; color: red; float: left">{{ $errors->first('name') }}</span>
                                                @endif
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class=" form-control-label">Телефон</label>
                                                <div class="input-group">
                                                    <input type="text" name="phone" class="form-control" value="{{$user->phone}}">
                                                    <div class="input-group-addon"><i class="fa fa-phone"></i></div>
                                                </div>  
                                                    @if($errors->has('phone'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('phone') }}</span>
                                                    @endif
                                            </div>
                                            @if($user->roles        )
                                                @foreach ($user->roles as $role)
                                                    @if($role->slug=='dostavka')
                                                        <div class="form-group">
                                                <label class=" form-control-label">Компания</label>
                                                <div class="input-group">
                                                    <select data-placeholder="" class="form-control" name ="company_id"tabindex="1">
                                                        @foreach($companies as $company)
                                                            <option value="{{ $company->id }}" @if($company->id == $user->company_id) selected @endif> {{ $company->name }}</option>
                                                        @endforeach
                                                 </select>
                                                    <div class="input-group-addon"><i class="fa fa-building"></i></div>
                                                </div>  
                                                    @if($errors->has('company_id'))
                                                        <span style="font-size: 10px; color: red; float: left">{{ $errors->first('company_id') }}</span>
                                                    @endif
                                            </div>
                                            @break
                                                    @endif
                                                @endforeach
                                            @endif    
                                            
                                            <div class="form-group">
                                                <label for="cash" class=" form-control-label">Денежные средства</label>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" name="cash" class="form-control" value="{{ $user->cash }}" disabled>
                                                            <div class="input-group-addon" >
                                                                <i class="fa fa-money"></i>
                                                            </div>
                                                        </div>  
                                                        @if($errors->has('cash'))
                                                            <span style="font-size: 10px; color: red; float: left">{{ $errors->first('cash') }}</span>
                                                         @endif
                                                     </div>
                                                    
                                                </div>
                                                
                                            </div>
                                            <br>
                                            <div class="row form-group">
                                                <div class="col-12 col-sm-9">
                                                    <button type="submit" class="btn btn-primary">Обновить</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="row">
                                    <div class="col-md-7"></div>
                                    <div class="col-md-3">
                                        @include('admin.modals.user_payment',['user' => $user])
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="card">
                        
                        <div class="card-body">
                            <div class="card-body card-block">
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="">
                                                    <h4 ><strong>Добавить роль</strong></h4>
                                                    <br>
                                                    @foreach (\App\Role::all() as $role)
                                                        
                                                        <a class="btn btn-success" href="{{route('admin.users.role_attach',['id'=>$role->id,'user'=>$user->id])}}">{{$role->name}}</a><br><br>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="">
                                                    <h4 ><strong>Удалить роль</strong></h4>
                                                    <br>
                                                        @if($user->roles        )
                                                            @foreach ($user->roles as $role)
                                                                <a class="btn btn-danger" href="{{route('admin.users.role_detach',['id'=>$role->id,'user'=>$user->id])}}">{{$role->name}}</a><br><br>
                                                            @endforeach
                                                        @endif    
                                                </div>
                                               
                                            </div>
                                        
                                        </div>
                                    </div>
                                </div>
                              
                            </div>
                        </div>
                    </div>                      
                </div>
            </div>
            
        </div><!-- .animated -->
    </div><!-- .content -->

    <div class="clearfix"></div>

</div><!-- /#right-panel -->

@endsection
@section('script')
<script src="{{ asset('admin/js/lib/chosen/chosen.jquery.min.js')}}"></script>

<script>
    jQuery(document).ready(function() {
        jQuery(".standardSelect").chosen({
            disable_search_threshold: 10,
            no_results_text: "Oops, nothing found!",
            width: "100%"
        });
    });
</script>
@endsection
