@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/lib/datatable/dataTables.bootstrap.min.css') }}">
    
    
@endsection
@section('content')
    <div id="right-panel" class="right-panel">
        @include('admin.includes.header')
        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                 <div class="row m-0">    
                    <div class="col-sm-8">
                        <div class="page-header float-left">
                            <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><strong><a href="#">Склад</a></strong></li>
                                <li class="">Таблица</li>
                            </ol>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>

        <div class="content">
            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Таблица данных</strong>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                           <th>№</th>
                                            <th>Имя</th>
                                            <th>Компания Имя</th>
                                            <th>Цена</th>
                                            <th>Количество</th>
                                            <th>Вес</th>
                                            <th>Настройка</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php($inc=1)
                                        @foreach($products as $product)
                                            <tr>
                                                <td>{{$inc++}}</td>
                                                <td>{{$product->name}}</td>
                                               
                                                <td>
                                                    @if($product->company) {{$product->company->name}} 
                                                    @else no company
                                                    @endif
                                                </td>
                                                
                                                <td>{{$product->price}}</td>
                                                <td>{{$product->quantity}}</td>
                                                <td>{{$product->weight}}</td>
                                                    <td class="text-center">
                                                
                                                        
                                                        @include('admin.modals.store_modal',['product' =>$product])
											        </td>
                                                  
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
       
        <div class="clearfix"></div>

        <!-- @include('admin.includes.footer') -->
    </div>
@endsection
@section('script')
    <script src="{{ asset('admin/js/lib/data-table/datatables.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/jszip.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/vfs_fonts.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.print.min.js') }}"></script>
    <script src="{{ asset('admin/js/lib/data-table/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('admin/js/init/datatables-init.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
         
      } );
    </script>
    
@endsection
